#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <config.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "e-conf_filesel.h"
#include "e-conf_file.h"
#include "capplet-widget.h"
#include <gnome.h>

#include "screen.xpm"
#include "move_box.xpm"
#include "move_lined.xpm"
#include "move_opaque.xpm"
#include "move_semi-solid.xpm"
#include "move_shaded.xpm"
#include "move_translucent.xpm"
#include "resize_box.xpm"
#include "resize_lined.xpm"
#include "resize_opaque.xpm"
#include "resize_semi-solid.xpm"
#include "resize_shaded.xpm"
#include "focus_click.xpm"
#include "focus_sloppy.xpm"
#include "focus_pointer.xpm"
#include "focus_and_display.xpm"
#include "special_effects.xpm"
#include "behavior.xpm"
#include "desktops.xpm"
#include "backgrounds.xpm"
#include "audio.xpm"
#include "fonts.xpm"
#include "colours.xpm"
#include "cursors.xpm"
#include "borders.xpm"
#include "menus.xpm"
#include "e_logo.xpm"

typedef struct _modcurve
{
  gint          num;
  guchar       *px;
  guchar       *py;
  guchar        map[256];
}
ModCurve;

typedef struct _colormodifierclass
{
  gchar              *name;
  ModCurve            red, green, blue;
}
ColorModifierClass;

typedef struct _background
  {
    gchar              *name;
    GtkWidget          *pmap;
    GtkWidget          *gpmap;
    GtkWidget          *box;
    GtkWidget          *gbox;
    GtkWidget          *button;
    GtkWidget          *table;
    struct _bg
      {
	GdkImlibColor       solid;
	gchar               *file;
	gint                 tile;
	gint                 keep_aspect;
	gint                 xjust, yjust;
	gint                 xperc, yperc;
      }
    bg;
    struct _top
      {
	gchar               *file;
	gint                 keep_aspect;
	gint                 xjust, yjust;
	gint                 xperc, yperc;
      }
    top;
    gchar changed;
    ColorModifierClass *cm;
  }
Background;

typedef struct _iconoption
  {
    gchar              *name;
    gchar             **icon_data;
    gchar              *tip;
  }
IconOption;

typedef struct _actionopt
{
  gchar *text;
  gint   id;
  gchar  param_tpe;
  gchar *params;
}
ActionOpt;

typedef struct _keybind
{
  char *key;
  gint  modifier;
  gint  id;
  char *params;
  gint  action_id;
}
Keybind;

GtkWidget          *current_capplet;
gchar               e_opt_sound = 1;
gchar               e_opt_slide_cleanup = 1;
gchar               e_opt_slide_map = 1;
gchar               e_opt_slide_desk = 1;
gchar               e_opt_hq_background = 1;
gchar               e_opt_saveunders = 1;
gint                e_opt_focus = 1;
gint                e_opt_move = 0;
gint                e_opt_resize = 2;
gint                e_opt_slide_mode = 0;
gchar               e_opt_tooltips = 1;
gchar               e_opt_menuslide = 0;
gfloat              e_opt_tooltiptime = 1.5;
gfloat              e_opt_shade_speed = 4000.0;
gfloat              e_opt_map_speed = 4000.0;
gfloat              e_opt_cleanup_speed = 4000.0;
gfloat              e_opt_desk_speed = 4000.0;
gfloat              e_opt_desktop_bg_timeout = 60.0;
gfloat              e_opt_number_of_desks = 32;
gfloat              e_opt_area_width = 3;
gfloat              e_opt_area_height = 3;
gchar               e_opt_autorase = 0;
gfloat              e_opt_autoraisetime = 0.5;
gint                e_opt_dragdir = 2;
gint                e_opt_dragbarwidth = 16;
gchar               e_opt_show_icons = 1;
gchar               e_opt_transients_follow_leader = 1;
gchar               e_opt_switch_for_transient_map = 1;
gchar               e_opt_all_new_windows_get_focus = 0;
gchar               e_opt_new_transients_get_focus = 0;
gchar               e_opt_e_opt_new_transients_get_focus_if_group_focused = 1;

gchar               e_opt_manual_placement = 0;
gchar               e_opt_raise_on_focus = 1;
gchar               e_opt_warp_on_focus = 0;
gfloat              e_opt_edge_flip_resistance = 0;

GList              *backgrounds = NULL;
Background         *deskbg[32];
gchar               deskbgchanged[32];
GList              *themes = NULL;
GList              *keys = NULL;
gint                origtheme = 0;
gint                curtheme = 0;

gint                curdesk = 0;
gchar               got_e_ipc_msg = 0;
gchar              *e_ipc_msg = NULL;
gchar               in_init = 1;
gchar               is_capp = 1;

static GtkWidget *global_ok = NULL;
static GtkWidget *global_try = NULL;
static GtkWidget *progress_win = NULL;
static GtkWidget *progress = NULL;
static GtkWidget *desk_option_menu = NULL;
static GtkWidget *desk_mini_display_area = NULL;
static GtkWidget *desk_select_menu = NULL;
static GtkWidget *bg_name_entry = NULL;
static GtkWidget *bg_color_area = NULL;
static GtkWidget *bg_range_area = NULL;
static GtkWidget *bg_bg_file_entry = NULL;
static GtkWidget *bg_fg_file_entry = NULL;
static GtkWidget *bg_bg_tile = NULL;
static GtkWidget *bg_bg_maxv = NULL;
static GtkWidget *bg_bg_maxh = NULL;
static GtkWidget *bg_bg_aspect = NULL;
static GtkWidget *bg_fg_pos = NULL;
static GtkWidget *bg_fg_maxv = NULL;
static GtkWidget *bg_fg_maxh = NULL;
static GtkWidget *bg_fg_aspect = NULL;
static GtkWidget *note_solid1 = NULL;
static GtkWidget *note_solid2 = NULL;
static GtkWidget *note_grad1 = NULL;
static GtkWidget *note_grad2 = NULL;
static GtkWidget *note_bg1 = NULL;
static GtkWidget *note_bg2 = NULL;
static GtkWidget *radio_image = NULL;
static GtkWidget *radio_gradient = NULL;
static GtkWidget *radio_color = NULL;
static GtkWidget *color_selector = NULL;
static GtkWidget *edit_win = NULL;
static GtkWidget *image_table_list = NULL;
static GtkWidget *bg_widget = NULL;
static GtkWidget *section_clist = NULL;
static gint       h1, h2, h3, h4, h5, h6, h7, h8, h9, h10;
static gint       table_size = 80;
static GtkWidget *act_ent1 = NULL;
static GtkWidget *act_ent2 = NULL;
static GtkWidget *act_ent3 = NULL;
static GtkWidget *act_ent4 = NULL;
static GtkWidget *act_list = NULL;
static GtkWidget *act_key = NULL;
static GtkWidget *act_mod = NULL;
static ActionOpt           actions[] =
{
    {N_("Run command"), 1, 1, NULL},
  
    {N_("Restart Enlightenment"), 7, 0, "restart"},
    {N_("Exit Enlightenment"), 7, 0, NULL},
  
    {N_("Goto Next Desktop"), 15, 0, NULL},
    {N_("Goto Previous Deskop"), 16, 0, NULL},
    {N_("Goto Desktop"), 42, 2, NULL},
    {N_("Raise Desktop"), 17, 0, NULL},
    {N_("Lower Desktop"), 18, 0, NULL},
    {N_("Reset Desktop In Place"), 21, 0, NULL},
  
    {N_("Toggle Deskrays"), 43, 0, NULL},
  
    {N_("Cleanup Windows"), 8, 0, NULL},
    {N_("Scroll Windows to left"), 48, 0, "-16 0"},
    {N_("Scroll Windows to right"), 48, 0, "16 0"},
    {N_("Scroll Windows up"), 48, 0, "0 -16"},
    {N_("Scroll Windows down"), 48, 0, "0 16"},
    {N_("Scroll Windows by [X Y] pixels"), 48, 3, NULL},
  
    {N_("Move mouse pointer to left"), 66, 0, "-1 0"},
    {N_("Move mouse pointer to right"), 66, 0, "1 0"},
    {N_("Move mouse pointer up"), 66, 0, "0 -1"},
    {N_("Move mouse pointer down"), 66, 0, "0 1"},
    {N_("Move mouse pointer by [X Y]"), 66, 3, NULL},
  
    {N_("Goto Desktop area [X Y]"), 62, 3, NULL},
    {N_("Move to Desktop area on the left"), 63, 0, "-1 0"},
    {N_("Move to Desktop area on the right"), 63, 0, "1 0"},
    {N_("Move to Desktop area above"), 63, 0, "0 -1"},
    {N_("Move to Desktop area below"), 63, 0, "0 1"},
  
    {N_("Raise Window"), 5, 0, NULL},
    {N_("Lower Window"), 6, 0, NULL},
    {N_("Close Window"), 13, 0, NULL},
    {N_("Annihilate Window"), 14, 0, NULL},
    {N_("Stick / Unstick Window"), 20, 0, NULL},
    {N_("Iconify Window"), 46, 0, NULL},
    {N_("Shade / Unshade Window"), 49, 0, NULL},
    {N_("Maximise Height of Window"), 50, 0, "conservative"},
    {N_("Maximise Height of Window to whole screen"), 50, 0, NULL},
    {N_("Maximise Height of Window toavailable space"), 50, 0, "available"},
    {N_("Maximise Width of Window"), 51, 0, "conservative"},
    {N_("Maximise Width of Window to whole screen"), 51, 0, NULL},
    {N_("Maximise Width of Window toavailable space"), 51, 0, "available"},
    {N_("Maximise Size of Window"), 52, 0, "conservative"},
    {N_("Maximise Size of Window to whole screen"), 52, 0, NULL},
    {N_("Maximise Size of Window toavailable space"), 52, 0, "available"},
    {N_("Send window to next desktop"), 53, 0, NULL},
    {N_("Send window to previous desktop"), 54, 0, NULL},
    {N_("Switch focus to next window"), 58, 0, NULL},
    {N_("Switch focus to previous window"), 59, 0, NULL},
    {N_("Glue / Unglue Window to Desktop screen"), 64, 0, NULL},
    {N_("Set Window layer to On Top"), 65, 0, "20"},
    {N_("Set Window layer to Above"), 65, 0, "6"},
    {N_("Set Window layer to Normal"), 65, 0, "4"},
    {N_("Set Window layer to Below"), 65, 0, "2"},
    {N_("Set Window layer"), 65, 2, NULL},
    {N_("Move Window to area on left"), 0, 0, "-1 0"},
    {N_("Move Window to area on right"), 0, 0, "1 0"},
    {N_("Move Window to area above"), 0, 0, "0 -1"},
    {N_("Move Window to area below"), 0, 0, "0 1"},
    {N_("Move Window by area [X Y]"), 0, 3, NULL},
  
    {N_("Set Window border style to the Default"), 69, 0, "DEFAULT"},
    {N_("Set Window border style to the Borderless"), 69, 0, "BORDERLESS"},
  
    {N_("Forget everything about Window"), 55, 0, "none"},
    {N_("Remember all Window settings"), 55, 0, NULL},
    {N_("Remember Window Border"), 55, 0, "border"},
    {N_("Remember Window Desktop"), 55, 0, "desktop"},
    {N_("Remember Window Desktop Area"), 55, 0, "area"},
    {N_("Remember Window Size"), 55, 0, "size"},
    {N_("Remember Window Location"), 55, 0, "location"},
    {N_("Remember Window Layer"), 55, 0, "layer"},
    {N_("Remember Window Stickyness"), 55, 0, "sticky"},
    {N_("Remember Window Shadedness"), 55, 0, "shade"},
  
    {N_("Show Root Menu"), 9, 0, "ROOT_2"},
    {N_("Show Winops Menu"), 9, 0, "WINOPS_MENU"},
    {N_("Show Named Menu"), 9, 1, NULL},

    {N_("Goto Linear Area"), 70, 2, NULL},
    {N_("Previous Linear Area"), 71, 0, "-1"},
    {N_("Next Linear Area"), 71, 0, "1"},
    {NULL, 0, 0, NULL}
};

gchar *mod_str[] =
{
  "",
  N_("CTRL"),
  N_("ALT"),
  N_("SHIFT"),
  N_("CTRL+ALT"),
  N_("CTRL+SHIFT"),
  N_("ALT+SHIFT"),
  N_("CTRL+ALT+SHIFT"),
  N_("WIN"),
  N_("MOD3"),
  N_("MOD4"),
  N_("MOD5"),
  N_("WIN+SHIFT"),
  N_("WIN+CTRL"),
  N_("WIN+ALT"),
  N_("MOD4+SHIFT"),
  N_("MOD4+CTRL"),
  N_("MOD4+ALT"),
  N_("MOD4+CTRL+SHIFT"),
  N_("MOD5+SHIFT"),
  N_("MOD5+CTRL")/*,*/
/*  N_("MOD5+CTRL+SHIFT")*/
};


gint                CommsInit(void (*msg_recieve_func) (gchar * msg));
void                CommsSend(gchar * s);
void                CreateCurve(ModCurve * c);
ColorModifierClass *CreateCMClass(char *name,
				  int rnum, unsigned char *rpx, unsigned char *rpy,
				  int gnum, unsigned char *gpx, unsigned char *gpy,
				  int bnum, unsigned char *bpx, unsigned char *bpy);
static void         recieve_ipc_msg(gchar * msg);
static gchar       *wait_for_ipc_msg(void);
static gchar       *get_line(gchar * str, int num);
static void         e_try(void);
static void         e_revert(void);
static void         e_ok(void);
static void         e_cancel(void);
static void         e_read(void);
static void         e_set_bg_to_e(Background *bg);
static void         e_render_bg_onto(GdkWindow * win, Background * bg);
static void         e_init_capplet(GtkWidget * c, GtkSignalFunc try, GtkSignalFunc revert, GtkSignalFunc ok, GtkSignalFunc cancel);
static void         e_cb_icon_option(GtkWidget * widget, gpointer data);
static GtkWidget   *e_create_icon_option(gchar * title, gint * option, gint width, gint num_options, IconOption * options);
static void         e_cb_onoff(GtkWidget * widget, gpointer data);
static GtkWidget   *e_create_onoff(gchar * text, gchar * value);
static void         e_cb_range(GtkWidget * widget, gpointer data);
static void         e_cb_rangeonoff_toggle(GtkWidget * widget, gpointer data);
static GtkWidget   *e_create_rangeonoff(gchar * text, gfloat * value, gfloat lower, gfloat upper, gchar * lower_text, gchar * upper_text, gchar * onoff, gfloat offvalue);
static void         e_cb_area_redraw(GtkWidget * widget, gpointer data);
static void         e_cb_desk_redraw(GtkWidget * widget, gpointer data);
static void         e_cb_area_range(GtkWidget * widget, gpointer data);
static GtkWidget   *e_areas_config(void);
static void         e_cb_desk_range(GtkWidget * widget, gpointer data);
static void         e_rebuild_desk_menu(void);
static void         redo_bg(Background *bg);
static GtkWidget   *e_desktops_config(void);
static GtkWidget   *__setup_pane_1(void);
static GtkWidget   *__setup_pane_2(void);
static GtkWidget   *__setup_pane_3(void);
static GtkWidget   *__setup_pane_4(void);
static GtkWidget   *__setup_pane_5(void);
static GtkWidget   *__setup_pane_6(void);
static GtkWidget   *__setup_pane_7(void);
static GtkWidget   *__setup_pane_8(void);
static GtkWidget   *__setup_pane_9(void);
static GtkWidget   *__setup_pane_10(void);
static GtkWidget   *__setup_pane_11(void);
static GtkWidget   *__setup_pane_12(void);
static GtkWidget   *__setup_pane_13(void);
static void         e_cb_list_click(GtkWidget * widget, gint num);
static void         e_setup(GtkWidget * c);
static void         e_setup_multi(GtkWidget * old, GtkWidget * c);
void                ModifyCMClass(ColorModifierClass *cm,
				  int rnum, unsigned char *rpx, 
				  unsigned char *rpy,
				  int gnum, unsigned char *gpx, 
				  unsigned char *gpy,
				  int bnum, unsigned char *bpx, 
				  unsigned char *bpy);
void                e_changed(GtkWidget *w, gboolean bool);
int                 main(int argc, char **argv);


void
CreateCurve(ModCurve * c)
{
  gint                 i, j, cx, v1, v2, val, dist;
  
  if (c->num == 0)
    {
      for (i = 0; i < 256; i++)
	c->map[i] = i;
      return;
    }
  cx = 0;
  c->map[cx++] = c->py[0];
  for (i = 1; i < c->num; i++)
    {
      v1 = c->py[i - 1];
      v2 = c->py[i];
      dist = c->px[i] - c->px[i - 1];
      if (dist < 2)
	c->map[cx++] = v2;
      else
	{
	  for (j = 0; j < dist; j++)
	    {
	      val = ((v2 * j) + (v1 * (dist - j - 1))) / (dist - 1);
	      c->map[cx++] = (guchar)val;
	    }
	}
    }
}

ColorModifierClass *
CreateCMClass(char *name,
	      int rnum, unsigned char *rpx, unsigned char *rpy,
	      int gnum, unsigned char *gpx, unsigned char *gpy,
	      int bnum, unsigned char *bpx, unsigned char *bpy)
{
  ColorModifierClass *cm;
  
  cm = g_malloc(sizeof(ColorModifierClass));
  cm->name = g_strdup(name);
  cm->red.px = NULL;
  cm->red.py = NULL;
  cm->green.px = NULL;
  cm->green.py = NULL;
  cm->blue.px = NULL;
  cm->blue.py = NULL;
  if (rnum < 2)
    cm->red.num = 0;
  else
    {
      cm->red.num = rnum;
      cm->red.px = g_malloc(rnum);
      memcpy(cm->red.px, rpx, rnum);
      cm->red.py = g_malloc(rnum);
      memcpy(cm->red.py, rpy, rnum);
    }
  if (gnum < 2)
    cm->green.num = 0;
  else
    {
      cm->green.num = gnum;
      cm->green.px = g_malloc(gnum);
      memcpy(cm->green.px, gpx, gnum);
      cm->green.py = g_malloc(gnum);
      memcpy(cm->green.py, gpy, gnum);
    }
  if (bnum < 2)
    cm->blue.num = 0;
  else
    {
      cm->blue.num = bnum;
      cm->blue.px = g_malloc(bnum);
      memcpy(cm->blue.px, bpx, bnum);
      cm->blue.py = g_malloc(bnum);
      memcpy(cm->blue.py, bpy, bnum);
    }
  CreateCurve(&(cm->red));
  CreateCurve(&(cm->green));
  CreateCurve(&(cm->blue));
  return cm;
}

void
ModifyCMClass(ColorModifierClass *cm,
	      int rnum, unsigned char *rpx, unsigned char *rpy,
	      int gnum, unsigned char *gpx, unsigned char *gpy,
	      int bnum, unsigned char *bpx, unsigned char *bpy)
{
  if (!cm)
    return;

  if (cm->red.px)
    g_free(cm->red.px);
  if (cm->red.py)
    g_free(cm->red.py);
  if (cm->green.px)
    g_free(cm->green.px);
  if (cm->green.py)
    g_free(cm->green.py);
  if (cm->blue.px)
    g_free(cm->blue.px);
  if (cm->blue.py)
    g_free(cm->blue.py);
  cm->red.px = NULL;
  cm->red.py = NULL;
  cm->green.px = NULL;
  cm->green.py = NULL;
  cm->blue.px = NULL;
  cm->blue.py = NULL;
  if (rnum < 2)
    cm->red.num = 0;
  else
    {
      cm->red.num = rnum;
      cm->red.px = g_malloc(rnum);
      memcpy(cm->red.px, rpx, rnum);
      cm->red.py = g_malloc(rnum);
      memcpy(cm->red.py, rpy, rnum);
    }
  if (gnum < 2)
    cm->green.num = 0;
  else
    {
      cm->green.num = gnum;
      cm->green.px = g_malloc(gnum);
      memcpy(cm->green.px, gpx, gnum);
      cm->green.py = g_malloc(gnum);
      memcpy(cm->green.py, gpy, gnum);
    }
  if (bnum < 2)
    cm->blue.num = 0;
  else
    {
      cm->blue.num = bnum;
      cm->blue.px = g_malloc(bnum);
      memcpy(cm->blue.px, bpx, bnum);
      cm->blue.py = g_malloc(bnum);
      memcpy(cm->blue.py, bpy, bnum);
    }
  CreateCurve(&(cm->red));
  CreateCurve(&(cm->green));
  CreateCurve(&(cm->blue));
}

static void
recieve_ipc_msg(gchar * msg)
{
  gdk_flush();
  e_ipc_msg = g_strdup(msg);
/* */
  gtk_main_quit();
/*  got_e_ipc_msg = 1;*/
}

static gchar       *
wait_for_ipc_msg(void)
{
/* */
/*  while (!got_e_ipc_msg)
    {
      while (gtk_events_pending())
	gtk_main_iteration();
      gdk_flush();
    }
 */
  gtk_main();
/*  got_e_ipc_msg = 0;*/
  return e_ipc_msg;
}

static gchar       *
get_line(gchar * str, int num)
{
  gchar              *s1, *s2, *s;
  gint                i, count, l;

  i = 0;
  count = 0;
  s1 = str;
  if (*str == '\n')
    i = 1;
  s2 = NULL;
  for (i = 0;; i++)
    {
      if ((str[i] == '\n') || (str[i] == 0))
	{
	  s2 = &(str[i]);
	  if ((count == num) && (s2 > s1))
	    {
	      l = s2 - s1;
	      s = g_malloc(l + 1);
	      strncpy(s, s1, l);
	      s[l] = 0;
	      return s;
	    }
	  count++;
	  if (str[i] == 0)
	    return NULL;
	  s1 = s2 + 1;
	}
    }
}

static void
e_try(void)
{
  gchar               cmd[4096];

  g_snprintf(cmd, sizeof(cmd), "set_controls"
	     " SOUND: %i"
	     " CLEANUPSLIDE: %i"
	     " MAPSLIDE: %i"
	     " DESKSLIDEIN: %i"
	     " HIQUALITYBG: %i"
	     " FOCUSMODE: %i"
	     " MOVEMODE: %i"
	     " RESIZEMODE: %i"
	     " SLIDEMODE: %i"
	     " TOOLTIPS: %i"
	     " TIPTIME: %f"
	     " SHADESPEED: %i"
	     " SLIDESPEEDMAP: %i"
	     " SLIDESPEEDCLEANUP: %i"
	     " DESKSLIDESPEED: %i"
	     " DESKTOPBGTIMEOUT: %i"
	     " SAVEUNDER: %i"
	     " NUMDESKTOPS: %i"
	     " MENUSLIDE: %i"
	     " AREA_SIZE: %i %i"
	     " AUTORAISE: %i"
	     " AUTORAISETIME: %f"
	     " DRAGDIR: %i"
	     " DRAGBARWIDTH: %i"
	     " SHOWICONS: %i"
	     " TRANSIENTSFOLLOWLEADER: %i"
	     " SWITCHFORTRANSIENTMAP: %i"
	     " ALL_NEW_WINDOWS_GET_FOCUS: %i"
	     " NEW_TRANSIENTS_GET_FOCUS: %i"
	     " NEW_TRANSIENTS_GET_FOCUS_IF_GROUP_FOCUSED: %i"
	     " MANUAL_PLACEMENT: %i"
	     " RAISE_ON_NEXT_FOCUS: %i"
	     " WARP_ON_NEXT_FOCUS: %i"
	     " EDGE_FLIP_RESISTANCE: %i"
	     ,
	     (gint)e_opt_sound,
	     (gint)e_opt_slide_cleanup, 
	     (gint)e_opt_slide_map,
	     (gint)e_opt_slide_desk, 
	     (gint)e_opt_hq_background,
	     (gint)e_opt_focus, 
	     (gint)e_opt_move, 
	     (gint)e_opt_resize, 
	     (gint)e_opt_slide_mode,
	     (gint)e_opt_tooltips, 
	     (gfloat)e_opt_tooltiptime,
	     (gint)e_opt_shade_speed,
	     (gint)e_opt_map_speed,
	     (gint)e_opt_cleanup_speed,
	     (gint)e_opt_desk_speed,
	     ((gint)e_opt_desktop_bg_timeout) * 60,
	     (gint)e_opt_saveunders,
	     (gint)e_opt_number_of_desks,
	     (gint)e_opt_menuslide,
	     (gint)e_opt_area_width,
	     (gint)e_opt_area_height,
	     (gint)e_opt_autorase,
	     (gfloat)e_opt_autoraisetime,
	     (gint)e_opt_dragdir,
	     (gint)e_opt_dragbarwidth,
	     (gint)e_opt_show_icons,
	     (gint)e_opt_transients_follow_leader,
	     (gint)e_opt_switch_for_transient_map,
	     (gint)e_opt_all_new_windows_get_focus,
	     (gint)e_opt_new_transients_get_focus,
	     (gint)e_opt_e_opt_new_transients_get_focus_if_group_focused,
	     (gint)e_opt_manual_placement,
	     (gint)e_opt_raise_on_focus,
	     (gint)e_opt_warp_on_focus,
	     (gint)e_opt_edge_flip_resistance
	     );
  CommsSend(cmd);
  {
    gint                i, d, l;
    Background         *bg;
    gchar               buf[256];
    gchar               changed;
    GList              *ptr;

    l = g_list_length(backgrounds);
    ptr = backgrounds;
    for (i = 0; ((i < l) && (ptr)); i++)
      {
	bg = ptr->data;
	ptr = ptr->next;
	changed = 0;
	if (bg)
	  {
	    gint                j;
	    
	    if (bg->changed)
	      {
		e_set_bg_to_e(bg);
		bg->changed = 0;
	      }
	    g_snprintf(cmd, sizeof(cmd), "use_bg %s",
		       bg->name);
	    for (j = 0; j < 32; j++)
	      {
		if (deskbg[j] == bg)
		  {
		    g_snprintf(buf, sizeof(buf), " %i", j);
		    strcat(cmd, buf);
		  }
		if (deskbgchanged[j])
		  changed = 1;
	      }
	    if (changed)
	      CommsSend(cmd);
	  }
      }
    changed = 0;
    g_snprintf(cmd, sizeof(cmd), "use_no_bg ");
    for (i = 0; i < 32; i++)
      {
	if ((deskbgchanged[i]) && (!deskbg[i]))
	  {
	    g_snprintf(buf, sizeof(buf), " %i", i);
	    strcat(cmd, buf);
	    changed = 1;
	  }
      }
    if (changed)
      CommsSend(cmd);
    for (i = 0; i < 32; i++)
      deskbgchanged[i] = 0;
  }
  if (keys)
    {
      GList *ptr;
      gchar *buf = NULL;
      
      ptr = keys;
      buf = g_strdup("set_keybindings ");
      while(ptr)
	{
	  Keybind *kb;
	  gchar    buf2[1024];
	  
	  kb = ptr->data;
	  ptr = ptr->next;
	  if (strcmp(kb->key, "EDIT ME"))
	    {
	      if (kb->params)
		g_snprintf(buf2, sizeof(buf2), "%s %i %i %s\n",
			   kb->key, kb->modifier, kb->id, kb->params);
	      else
		g_snprintf(buf2, sizeof(buf2), "%s %i %i\n",
			   kb->key, kb->modifier, kb->id);
	      buf = g_realloc(buf, strlen(buf) + strlen(buf2) + 1);
	      strcat(buf, buf2);
	    }
	}
      CommsSend(buf);
      g_free(buf);
    }
  if (origtheme != curtheme)
    {
      g_snprintf(cmd, sizeof(cmd), "set_default_theme %s", 
		 (gchar *)((g_list_nth(themes, curtheme))->data));
      CommsSend(cmd);
      origtheme = curtheme;
    }
  CommsSend("save_config");
  if (global_try)
    gtk_widget_set_sensitive(global_try, FALSE);  
}

static void
e_revert(void)
{
}

static void
e_ok(void)
{
  e_try();
  gtk_main_quit();
}

static void
e_cancel(void)
{
  gtk_main_quit();
}

static void
e_read(void)
{
  gchar               cmd[4096];
  gchar              *buf;
  gchar              *msg, *def;
  gint                i, j, k;
  Keybind            *kb;
  
  CommsSend("get_default_theme");
  def = wait_for_ipc_msg();
  
  CommsSend("list_themes");
  msg = wait_for_ipc_msg();
  i = 0;
  j = 0;
  while ((buf = get_line(msg, i++)))
    {
      if (!strcmp(def, buf))
	curtheme = j;
      themes = g_list_append(themes, buf);
      j++;
    }
  origtheme = curtheme;
  g_free(msg);
  g_free(def);

  CommsSend("get_keybindings");
  msg = wait_for_ipc_msg();
  i = 0;
  while ((buf = get_line(msg, i++)))
    {
      if (strlen(buf) < 1)
	break;
      kb = g_malloc(sizeof(Keybind));
      sscanf(buf, "%1000s", cmd);
      kb->key = g_strdup(cmd);
      sscanf(buf, "%*s %i", &j);
      kb->modifier = j;
      sscanf(buf, "%*s %*s %i", &j);
      kb->id = j;
      if (atword(buf, 4))
	kb->params = g_strdup(atword(buf, 4));
      else
	kb->params = NULL;
      kb->action_id = -1;
      for (k = 0; ((actions[k].text) && (kb->action_id < 0)); k++)
	{
	  if (kb->id == actions[k].id)
	    {
	      if (kb->params)
		{
		  if ((actions[k].param_tpe == 0) && (actions[k].params))
		    {
		      if (!strcmp(kb->params, actions[k].params))
			kb->action_id = k;
		    }
		  else
		    kb->action_id = k;
		}
	      else if (!actions[k].params)
		kb->action_id = k;
	    }
	}
      if (kb->action_id < 0)
	{
	  if (kb->key)
	    g_free(kb->key);
	  if (kb->params)
	    g_free(kb->params);
	  g_free(kb);
	}
      else
	keys = g_list_append(keys, kb);
      g_free(buf);
    }
  g_free(msg);
  
  CommsSend("get_controls");
  msg = wait_for_ipc_msg();
  i = 0;
  while ((buf = get_line(msg, i++)))
    {
      sscanf(buf, "%4000s", cmd);
      if (!strcmp(cmd, "SOUND:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_sound = atoi(cmd);
	}
      else if (!strcmp(cmd, "CLEANUPSLIDE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_slide_cleanup = atoi(cmd);
	}
      else if (!strcmp(cmd, "MAPSLIDE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_slide_map = atoi(cmd);
	}
      else if (!strcmp(cmd, "DESKSLIDEIN:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_slide_desk = atoi(cmd);
	}
      else if (!strcmp(cmd, "HIQUALITYBG:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_hq_background = atoi(cmd);
	}
      else if (!strcmp(cmd, "FOCUSMODE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_focus = atoi(cmd);
	}
      else if (!strcmp(cmd, "MOVEMODE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_move = atoi(cmd);
	}
      else if (!strcmp(cmd, "RESIZEMODE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_resize = atoi(cmd);
	}
      else if (!strcmp(cmd, "SLIDEMODE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_slide_mode = atoi(cmd);
	}
      else if (!strcmp(cmd, "TOOLTIPS:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_tooltips = atoi(cmd);
	}
      else if (!strcmp(cmd, "TIPTIME:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_tooltiptime = atof(cmd);
	}
      else if (!strcmp(cmd, "SHADESPEED:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_shade_speed = atof(cmd);
	}
      else if (!strcmp(cmd, "SLIDESPEEDMAP:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_map_speed = atof(cmd);
	}
      else if (!strcmp(cmd, "SLIDESPEEDCLEANUP:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_cleanup_speed = atof(cmd);
	}
      else if (!strcmp(cmd, "DESKSLIDESPEED:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_desk_speed = atof(cmd);
	}
      else if (!strcmp(cmd, "DESKTOPBGTIMEOUT:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_desktop_bg_timeout = atof(cmd) / 60;
	}
      else if (!strcmp(cmd, "SAVEUNDER:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_saveunders = atoi(cmd);
	}
      else if (!strcmp(cmd, "NUMDESKTOPS:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_number_of_desks = atof(cmd);
	}
      else if (!strcmp(cmd, "MENUSLIDE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_menuslide = atof(cmd);
	}
      else if (!strcmp(cmd, "AREA_SIZE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_area_width = atof(cmd);
	  sscanf(buf, "%*s %*s %4000s", cmd);
	  e_opt_area_height = atof(cmd);
	}
      
      else if (!strcmp(cmd, "AUTORAISE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_autorase = atoi(cmd);
	}
      else if (!strcmp(cmd, "AUTORAISETIME:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_autoraisetime = atof(cmd);
	}
      else if (!strcmp(cmd, "DRAGDIR:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_dragdir = atoi(cmd);
	}
      else if (!strcmp(cmd, "DRAGBARWIDTH:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_dragbarwidth = atoi(cmd);
	}
      else if (!strcmp(cmd, "SHOWICONS:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_show_icons = atoi(cmd);
	}
      else if (!strcmp(cmd, "TRANSIENTSFOLLOWLEADER:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_transients_follow_leader = atoi(cmd);
	}
      else if (!strcmp(cmd, "SWITCHFORTRANSIENTMAP:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_switch_for_transient_map = atoi(cmd);
	}
      else if (!strcmp(cmd, "ALL_NEW_WINDOWS_GET_FOCUS:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_all_new_windows_get_focus = atoi(cmd);
	}
      else if (!strcmp(cmd, "NEW_TRANSIENTS_GET_FOCUS:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_new_transients_get_focus = atoi(cmd);
	}
      else if (!strcmp(cmd, "NEW_TRANSIENTS_GET_FOCUS_IF_GROUP_FOCUSED:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_e_opt_new_transients_get_focus_if_group_focused = atoi(cmd);
	}
      else if (!strcmp(cmd, "MANUAL_PLACEMENT:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_manual_placement = atoi(cmd);
	}
      else if (!strcmp(cmd, "RAISE_ON_NEXT_FOCUS:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_raise_on_focus = atoi(cmd);
	}
      else if (!strcmp(cmd, "WARP_ON_NEXT_FOCUS:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_warp_on_focus = atoi(cmd);
	}
      else if (!strcmp(cmd, "EDGE_FLIP_RESISTANCE:"))
	{
	  sscanf(buf, "%*s %4000s", cmd);
	  e_opt_edge_flip_resistance = atof(cmd);
	}
      g_free(buf);
    }
  g_free(msg);

  CommsSend("list_bg");
  msg = wait_for_ipc_msg();
  i = 0;
  while ((buf = get_line(msg, i++)))
    {
      Background         *bg;
      gchar              *msg2;
      gchar               b1[4096], b2[4096];

      if (strlen(buf) > 0)
	{
	  bg = g_malloc(sizeof(Background));
	  bg->name = g_strdup(buf);
	  bg->pmap = NULL;
	  bg->cm = NULL;
	  bg->changed = 0;
	  backgrounds = g_list_append(backgrounds, bg);
	  g_snprintf(cmd, sizeof(cmd), "get_bg %s", bg->name);
	  CommsSend(cmd);
	  msg2 = wait_for_ipc_msg();
	  if (msg2)
	    {
	      sscanf(msg2, "%*s %i %i %i %s %i %i %i %i %i %i %s %i %i %i %i %i",
		     &bg->bg.solid.r, &bg->bg.solid.g, &bg->bg.solid.b,
		     b1, &bg->bg.tile, &bg->bg.keep_aspect,
		     &bg->bg.xjust, &bg->bg.yjust, &bg->bg.xperc, &bg->bg.yperc,
		     b2, &bg->top.keep_aspect, &bg->top.xjust,
		     &bg->top.yjust, &bg->top.xperc, &bg->top.yperc);
	      if (!strcmp(b1, "(null)"))
		bg->bg.file = NULL;
	      else
		bg->bg.file = g_strdup(b1);
	      if (!strcmp(b2, "(null)"))
		bg->top.file = NULL;
	      else
		bg->top.file = g_strdup(b2);
	      g_free(msg2);
	    }
	}
      g_free(buf);
    }
  g_free(msg);
    {
      gint                ii, j, d, jj, k;
      Background         *bg;
      GList              *ptr;
      
      j = g_list_length(backgrounds);
      ptr = backgrounds;
      for (i = 0; ((i < j) && (ptr)); i++)
	{
	  if (progress)
	    gtk_progress_bar_update(GTK_PROGRESS_BAR(progress),
				    (gfloat) i / (gfloat) j);
	  bg = ptr->data;
	  ptr = ptr->next;
	  if (bg)
	    {
	      g_snprintf(cmd, sizeof(cmd), "uses_bg %s", bg->name);
	      CommsSend(cmd);
	      msg = wait_for_ipc_msg();
	      ii = 0;
	      while ((buf = get_line(msg, ii++)))
		{
		  if (sscanf(buf, "%i", &d) > 0)
		    {
		      if (d < 0)
			d = 0;
		      if (d > 31)
			d = 31;
		      deskbg[d] = bg;
		    }
		  g_free(buf);
		}
	      g_free(msg);
	      g_snprintf(cmd, sizeof(cmd), "get_bg_colmod %s", bg->name);
	      CommsSend(cmd);
	      msg = wait_for_ipc_msg();
	      if (strcmp(msg, "(null)"))
		{
		  gchar       *name, w[4096];
		  gint         rnum = 0, gnum = 0, bnum = 0;
		  guchar      *rpx = NULL, *rpy = NULL;
		  guchar      *gpx = NULL, *gpy = NULL;
		  guchar      *bpx = NULL, *bpy = NULL;
		  
		  name = g_strdup(msg);
		  g_free(msg);
		  g_snprintf(cmd, sizeof(cmd), "get_colmod %s", name);
		  CommsSend(cmd);
		  msg = wait_for_ipc_msg();

		  buf = get_line(msg, 0);
		  word(buf, 1, w);
		  rnum = atoi(w);
		  rpx = g_malloc(rnum);
		  rpy = g_malloc(rnum);
		  ii = 2;
		  jj = 0;
		  while (jj < rnum)
		    {
		      word(buf, ii++, w);
		      k = atoi(w);
		      rpx[jj] = k;
		      word(buf, ii++, w);
		      k = atoi(w);
		      rpy[jj++] = k;
		    }
                  g_free(buf);

		  buf = get_line(msg, 1);
		  word(buf, 1, w);
		  gnum = atoi(w);
		  gpx = g_malloc(gnum);
		  gpy = g_malloc(gnum);
		  ii = 2;
		  jj = 0;
		  while (jj < gnum)
		    {
		      word(buf, ii++, w);
		      k = atoi(w);
		      gpx[jj] = k;
		      word(buf, ii++, w);
		      k = atoi(w);
		      gpy[jj++] = k;
		    }
                  g_free(buf);

		  buf = get_line(msg, 2);
		  word(buf, 1, w);
		  bnum = atoi(w);
		  bpx = g_malloc(bnum);
		  bpy = g_malloc(bnum);
		  ii = 2;
		  jj = 0;
		  while (jj < bnum)
		    {
		      word(buf, ii++, w);
		      k = atoi(w);
		      bpx[jj] = k;
		      word(buf, ii++, w);
		      k = atoi(w);
		      bpy[jj++] = k;
		    }
                  g_free(buf);
		  g_free(msg);
		  
		  bg->cm = CreateCMClass(name, 
					 rnum, rpx, rpy, 
					 gnum, gpx, gpy, 
					 bnum, bpx, bpy);
		  g_free(name);
		  if (rpx)
		    g_free(rpx);
		  if (rpy)
		    g_free(rpy);
		  if (gpx)
		    g_free(gpx);
		  if (gpy)
		    g_free(gpy);
		  if (bpx)
		    g_free(bpx);
		  if (bpy)
		    g_free(bpy);
		}
	      else
		g_free(msg);		
	    }
	}
    }
}

static void
e_render_bg_onto(GdkWindow * win, Background * bg)
{
  gchar               cmd[4096];
  gchar              *msg;
  Window              last_rend = 0;
  gchar              *last_bg_name = NULL;

  if ((last_bg_name) && 
      (!strcmp(last_bg_name, bg->name)) && 
      (last_rend == GDK_WINDOW_XWINDOW(win)) &&
      (!bg->changed))
    {
      return;
    }
  g_snprintf(cmd, sizeof(cmd), "draw_bg_to %x %s",
	     (gint) GDK_WINDOW_XWINDOW(win), bg->name);
  CommsSend(cmd);
  msg = wait_for_ipc_msg();
  g_free(msg);
  if (last_bg_name)
    g_free(last_bg_name);
  last_bg_name = g_strdup(bg->name);
  last_rend = GDK_WINDOW_XWINDOW(win);
}

static void
e_cb_multi(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  gint *value;
  gint val;
  
  c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "capplet");
  e_changed(c, FALSE);
  val = (gint)gtk_object_get_data(GTK_OBJECT(widget), "value");
  value = (gint *)data;
  *value = val;
}

static GtkWidget *
e_add_multi_to_frame(gchar *text, gchar *opts, gint *value, gint vh)
{
  GtkWidget *label, *om, *m, *mi, *align, *box;
  gint i, j;
  gchar s[1024], tmpbuf[2];
  
  if (!text)
    return NULL;
  
  if (vh)
    box = gtk_vbox_new(FALSE, 1);
  else
    box = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(box);
  
  label = gtk_label_new(text);
  gtk_widget_show(label);
  align = gtk_alignment_new(0.0, 0.5, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(align), label);
  gtk_box_pack_start(GTK_BOX(box), align, FALSE, FALSE, 1);
  
  om = gtk_option_menu_new();
  if (vh)
    align = gtk_alignment_new(0.0, 0.5, 0.0, 0.0);
  else
    align = gtk_alignment_new(1.0, 0.5, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(align), om);
  gtk_box_pack_start(GTK_BOX(box), align, TRUE, TRUE, 1);
  
  m = gtk_menu_new();
  gtk_widget_show(m);
  
  i = 0;
  j = 0;
  tmpbuf[1] = 0;
  while (opts[j])
    {
      s[0] = 0;
      if (opts[j] == '\n')
	j++;
      while ((opts[j]) && (opts[j] != '\n'))
	{
	  tmpbuf[0] = opts[j];
	  strcat(s, tmpbuf);
	  j++;
	}
      mi = gtk_menu_item_new_with_label(s);
      gtk_object_set_data(GTK_OBJECT(mi), "value", (gpointer)i);
      gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer)current_capplet);  
      gtk_signal_connect(GTK_OBJECT(mi),
			 "activate", GTK_SIGNAL_FUNC(e_cb_multi),
			 (gpointer)value);
      gtk_menu_append(GTK_MENU(m), mi);
      gtk_widget_show(mi);
      i++;
    }
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), m);
  gtk_option_menu_set_history(GTK_OPTION_MENU(om), *value);
  gtk_widget_show (om);
  
  return box;
}


void
e_changed(GtkWidget *w, gboolean bool)
{
  if ((is_capp) && (w))
    capplet_widget_state_changed(CAPPLET_WIDGET(w), FALSE);
  else
    {
      if (global_ok)
	{
	  gtk_widget_set_sensitive(global_ok, TRUE);
	  gtk_widget_set_sensitive(global_try, TRUE);
	}
    }
}

static void
e_init_capplet(GtkWidget * c, GtkSignalFunc try, GtkSignalFunc revert,
	       GtkSignalFunc ok, GtkSignalFunc cancel)
{
  gtk_widget_show(c);

  if (is_capp)
    {
      gtk_signal_connect(GTK_OBJECT(c), "try",
			 GTK_SIGNAL_FUNC(try), NULL);
      gtk_signal_connect(GTK_OBJECT(c), "revert",
			 GTK_SIGNAL_FUNC(revert), NULL);
      gtk_signal_connect(GTK_OBJECT(c), "ok",
			 GTK_SIGNAL_FUNC(ok), NULL);
      gtk_signal_connect(GTK_OBJECT(c), "cancel",
			 GTK_SIGNAL_FUNC(cancel), NULL);
    }
  else
    {
      GtkWidget *vbox, *separator, *bbox, *frame;
      GtkWidget *tryb, *okb, *revertb, *cancelb;
      
      vbox = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(c), "vbox");
      separator = gtk_hseparator_new ();
      gtk_box_pack_start(GTK_BOX(vbox), separator, FALSE, FALSE, 1);
      bbox = gtk_hbutton_box_new ();
      gtk_box_pack_start(GTK_BOX(vbox), bbox, FALSE, FALSE, 1);
      gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_SPREAD);
      gtk_button_box_set_spacing (GTK_BUTTON_BOX (bbox), 2);
      gtk_button_box_set_child_size (GTK_BUTTON_BOX (bbox), 2, -1);
      gtk_container_set_border_width (GTK_CONTAINER (bbox), 2);
      
      tryb = gtk_button_new_with_label (_("Apply"));
      gtk_container_add (GTK_CONTAINER (bbox), tryb);
      global_try = tryb;
/*
      revertb = gtk_button_new_with_label (_("Revert"));
      gtk_container_add (GTK_CONTAINER (bbox), revertb);
*/      
      okb = gtk_button_new_with_label (_("OK"));
      gtk_container_add (GTK_CONTAINER (bbox), okb);
      global_ok = okb;

      cancelb = gtk_button_new_with_label (_("Cancel"));
      gtk_container_add (GTK_CONTAINER (bbox), cancelb);
      
      gtk_widget_set_sensitive(global_ok, FALSE);
      gtk_widget_set_sensitive(global_try, FALSE);

      gtk_signal_connect (GTK_OBJECT(tryb), "clicked", 
			  GTK_SIGNAL_FUNC(try), NULL);
 /*     gtk_signal_connect (GTK_OBJECT(revertb), "clicked", 
			  GTK_SIGNAL_FUNC(revert), NULL);*/
      gtk_signal_connect (GTK_OBJECT(okb), "clicked", 
			  GTK_SIGNAL_FUNC(ok), NULL);
      gtk_signal_connect (GTK_OBJECT(cancelb), "clicked", 
			  GTK_SIGNAL_FUNC(cancel), NULL);
      gtk_signal_connect(GTK_OBJECT(c), "delete_event",
			  GTK_SIGNAL_FUNC(cancel), NULL);
      gtk_signal_connect(GTK_OBJECT(c), "destroy",
			  GTK_SIGNAL_FUNC(cancel), NULL);

      gtk_widget_show_all(bbox);
      gtk_widget_show(separator);
    }
}

static void
e_cb_icon_option(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c;
  gint               *value;
  gint                val;

  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
  if (!in_init)
    e_changed((c), FALSE);
  val = (gint) gtk_object_get_data(GTK_OBJECT(widget), "value");
  value = (gint *) data;
  *value = val;
}

static GtkWidget   *
e_create_icon_option(gchar * title, gint * option, gint width,
		     gint num_options, IconOption * options)
{
  GSList             *items = NULL;
  GtkWidget          *frame, *align, *table, *pixmap, *radio, *label, *vbox;
  GtkWidget          *tip;
  GdkPixmap          *pmap, *mask;
  gint                i, j, k;

  frame = gtk_frame_new(title);
  gtk_widget_show(frame);

  align = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(frame), align);

  table = gtk_table_new(1, 1, TRUE);
  gtk_widget_show(table);
  gtk_container_set_border_width(GTK_CONTAINER(table), 1);
  gtk_container_add(GTK_CONTAINER(align), table);

  j = 0;
  k = 0;
  for (i = 0; i < num_options; i++)
    {
      pmap = NULL;
      mask = NULL;
      gdk_imlib_data_to_pixmap(options[i].icon_data, &pmap, &mask);
      radio = gtk_radio_button_new(items);
      gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(radio), FALSE);
      gtk_widget_show(radio);
      items = g_slist_append(items, radio);
      gtk_object_set_data(GTK_OBJECT(radio), "value", (gpointer) i);
      gtk_object_set_data(GTK_OBJECT(radio), "capplet", (gpointer) current_capplet);
      gtk_signal_connect(GTK_OBJECT(radio),
			 "clicked", GTK_SIGNAL_FUNC(e_cb_icon_option),
			 (gpointer) option);
      if (i == *option)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), 1);
      vbox = gtk_vbox_new(FALSE, 0);
      gtk_widget_show(vbox);

      pixmap = gtk_pixmap_new(pmap, mask);
      gtk_widget_show(pixmap);
      gtk_box_pack_start(GTK_BOX(vbox), pixmap, FALSE, FALSE, 1);

      label = gtk_label_new(_(options[i].name));
      gtk_widget_show(label);
      gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 1);
      
      gtk_container_add(GTK_CONTAINER(radio), vbox);
      
      gtk_table_attach(GTK_TABLE(table), radio, j, j + 1, k, k + 1,
		       GTK_EXPAND | GTK_FILL,
		       GTK_EXPAND | GTK_FILL, 1, 1);
      tip = (GtkWidget *) gtk_tooltips_new();
      gtk_tooltips_set_tip(GTK_TOOLTIPS(tip), radio, _(options[i].tip), NULL);
      gtk_tooltips_enable(GTK_TOOLTIPS(tip));
      j++;
      if (j >= width)
	{
	  j = 0;
	  k++;
	}
    }
  return frame;
}

/* on off widget */
static void
e_cb_onoff(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c;
  gchar              *value;
  
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
  if (!in_init)
    e_changed((c), FALSE);
  value = (gchar *) data;
  if (GTK_TOGGLE_BUTTON(widget)->active)
    *value = 1;
  else
    *value = 0;
}

static GtkWidget   *
e_create_onoff(gchar * text, gchar * value)
{
  GtkWidget          *check, *align;
  gint                rows;
  
  if (!text)
    return NULL;
  
  check = gtk_check_button_new_with_label(text);
  gtk_widget_show(check);
  align = gtk_alignment_new(0.0, 0.5, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(align), check);
  
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  if (*value)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 1);
  else
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 0);
  
  gtk_signal_connect(GTK_OBJECT(check), "toggled",
		     GTK_SIGNAL_FUNC(e_cb_onoff), (gpointer) value);
  return align;
}

/* range + on/off widget */
static void
e_cb_range(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c;
  GtkWidget          *w;
  GtkAdjustment      *adj;
  gfloat             *value, current_val;
  static gint         just_changed = 0;
  GtkFunction         func;

  w = GTK_WIDGET(data);
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "capplet");
  func = (GtkFunction) gtk_object_get_data(GTK_OBJECT(w), "function");
  if (!in_init)
    e_changed((c), FALSE);
  adj = gtk_object_get_data(GTK_OBJECT(w), "adj");
  value = (gfloat *) gtk_object_get_data(GTK_OBJECT(w), "value");
  if (adj->step_increment > 0.5)
    {
      current_val = adj->value;
      adj->value = (gfloat) ((gint) (adj->value / adj->step_increment)) *
	adj->step_increment;
      if (just_changed == 0)
	{
	  just_changed++;
	  gtk_adjustment_value_changed(adj);
	}
      else if (just_changed > 0)
	just_changed--;
    }
  *value = adj->value;

  if (func)
    (*func) ((gpointer) ((gint)adj->value));

  widget = NULL;
}

static void
e_cb_rangeonoff_toggle(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c;
  GtkWidget          *w, *ww;
  GtkAdjustment      *adj;
  gfloat             *value, offvalue;
  gchar              *onoff;

  w = GTK_WIDGET(data);
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "capplet");
  if (!in_init)
    e_changed((c), FALSE);
  adj = gtk_object_get_data(GTK_OBJECT(w), "adj");
  value = (gfloat *) gtk_object_get_data(GTK_OBJECT(w), "value");
  onoff = (gchar *) gtk_object_get_data(GTK_OBJECT(w), "onoff");
  offvalue = *((gfloat *) gtk_object_get_data(GTK_OBJECT(w), "offvalue"));
  *value = adj->value;

  if (onoff)
    {
      if (GTK_TOGGLE_BUTTON(widget)->active)
	{
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l1");
	  gtk_widget_set_sensitive(ww, TRUE);
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l2");
	  gtk_widget_set_sensitive(ww, TRUE);
	  gtk_widget_set_sensitive(w, TRUE);
	  *onoff = 1;
	}
      else
	{
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l1");
	  gtk_widget_set_sensitive(ww, FALSE);
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l2");
	  gtk_widget_set_sensitive(ww, FALSE);
	  gtk_widget_set_sensitive(w, FALSE);
	  *onoff = 0;
	}
    }
  else
    {
      if (GTK_TOGGLE_BUTTON(widget)->active)
	{
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l1");
	  gtk_widget_set_sensitive(ww, TRUE);
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l2");
	  gtk_widget_set_sensitive(ww, TRUE);
	  gtk_widget_set_sensitive(w, TRUE);
	  *value = 0.0;
	}
      else
	{
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l1");
	  gtk_widget_set_sensitive(ww, FALSE);
	  ww = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "l2");
	  gtk_widget_set_sensitive(ww, FALSE);
	  gtk_widget_set_sensitive(w, FALSE);
	  *value = offvalue;
	}
    }
  widget = NULL;
}

static GtkWidget   *
e_create_rangeonoff(gchar * text, gfloat * value,
		    gfloat lower, gfloat upper, gchar * lower_text,
		    gchar * upper_text, gchar * onoff, gfloat offvalue)
{
  GtkObject          *adj;
  GtkWidget          *frame, *hscale, *hbox, *label, *align, *align2, *check;
  GtkWidget          *label2, *w;
  gfloat             *offval;

  if (!text)
    return NULL;

  frame = gtk_frame_new(text);
  gtk_widget_show(frame);
  align = gtk_alignment_new(0.5, 0.5, 1.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(align), frame);

  w = align;

  align = gtk_alignment_new(0.5, 0.5, 1.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(frame), align);
  hbox = gtk_hbox_new(FALSE, 2);
  gtk_widget_show(hbox);
  gtk_container_add(GTK_CONTAINER(align), hbox);

  check = gtk_check_button_new_with_label(_("Enable"));
  gtk_widget_show(check);
  align2 = gtk_alignment_new(1.0, 1.0, 0.0, 0.0);
  gtk_widget_show(align2);
  gtk_container_add(GTK_CONTAINER(align2), check);
  gtk_box_pack_start(GTK_BOX(hbox), align2, FALSE, FALSE, 0);

  if (onoff)
    {
      if (*onoff)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 1);
      else
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 0);
    }
  else
    {
      if (*value == offvalue)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 0);
      else
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 1);
    }

  adj = gtk_adjustment_new(*value, lower, upper, 0.1, .1, 0.1);
  hscale = gtk_hscale_new(GTK_ADJUSTMENT(adj));
  gtk_widget_show(hscale);

  gtk_signal_connect(GTK_OBJECT(check), "toggled",
		  GTK_SIGNAL_FUNC(e_cb_rangeonoff_toggle), (gpointer) hscale);

  label = gtk_label_new(lower_text);
  gtk_widget_show(label);
  align2 = gtk_alignment_new(0.5, 1.0, 0.0, 0.0);
  gtk_widget_show(align2);
  gtk_container_add(GTK_CONTAINER(align2), label);
  gtk_box_pack_start(GTK_BOX(hbox), align2, FALSE, FALSE, 0);
  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		     GTK_SIGNAL_FUNC(e_cb_range), (gpointer) hscale);
  gtk_object_set_data(GTK_OBJECT(hscale), "adj", (gpointer) adj);
  gtk_object_set_data(GTK_OBJECT(hscale), "value", (gpointer) value);
  gtk_object_set_data(GTK_OBJECT(hscale), "capplet", (gpointer) current_capplet);
  gtk_object_set_data(GTK_OBJECT(hscale), "onoff", (gpointer) onoff);
  gtk_object_set_data(GTK_OBJECT(hscale), "l1", (gpointer) label);
  offval = g_malloc(sizeof(gfloat));
  *offval = offvalue;
  gtk_object_set_data(GTK_OBJECT(hscale), "offvalue", (gpointer) offval);

  gtk_box_pack_start(GTK_BOX(hbox), hscale, TRUE, TRUE, 0);
  label2 = label = gtk_label_new(upper_text);
  gtk_object_set_data(GTK_OBJECT(hscale), "l2", (gpointer) label);
  gtk_widget_show(label);
  align2 = gtk_alignment_new(0.5, 1.0, 0.0, 0.0);
  gtk_widget_show(align2);
  gtk_container_add(GTK_CONTAINER(align2), label);
  gtk_box_pack_start(GTK_BOX(hbox), align2, FALSE, FALSE, 0);
  if (GTK_TOGGLE_BUTTON(check)->active)
    {
      gtk_widget_set_sensitive(label, TRUE);
      gtk_widget_set_sensitive(hscale, TRUE);
      gtk_widget_set_sensitive(label2, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive(label, FALSE);
      gtk_widget_set_sensitive(hscale, FALSE);
      gtk_widget_set_sensitive(label2, FALSE);
    }
  return w;
}

static void
e_cb_area_redraw(GtkWidget * widget, gpointer data)
{
  GdkWindow          *win;
  GtkStyle           *s;
  gint                i, j, x, y, w, h, ww, hh;
  GdkImlibImage      *im = NULL;
  GdkImlibBorder      border =
  {4, 4, 4, 13};

  win = widget->window;
  s = widget->style;
  gdk_window_get_size(win, &ww, &hh);

  ww -= 40;
  hh -= 30;

  if (!im)
    {
      im = gdk_imlib_create_image_from_xpm_data(screen_xpm);
      gdk_imlib_set_image_border(im, &border);
    }
  gdk_window_clear(win);
  w = 20;
  h = 15;
  if (e_opt_area_height > e_opt_area_width)
    {
      w = ww / (gint) e_opt_area_height;
      h = hh / (gint) e_opt_area_height;
    }
  else
    {
      w = ww / (gint) e_opt_area_width;
      h = hh / (gint) e_opt_area_width;
    }
  for (j = 0; j < (gint) e_opt_area_height; j++)
    {
      for (i = 0; i < (gint) e_opt_area_width; i++)
	{
	  x = 20 + (i * w);
	  y = 15 + (j * h);
	  gtk_draw_box(s, win, GTK_STATE_ACTIVE, GTK_SHADOW_OUT, x, y, w, h);
	}
    }
  x = 20;
  y = 15;
  gdk_imlib_paste_image(im, win, x - 4, y - 4, w + 8, h + 17);

}

static void
e_cb_desk_redraw(GtkWidget * widget, gpointer data)
{
  GdkWindow          *win;
  GtkStyle           *s;
  gint                i, x, y, w, h, ww, hh, tw, th, dummy, ascent, descent;
  gchar               str[1024];

  win = widget->window;
  s = widget->style;
  gdk_window_get_size(win, &ww, &hh);

  ww -= 4;
  hh -= 4;

  gdk_window_clear(win);
  w = ww - ((e_opt_number_of_desks - 1) * 3);
  h = hh - ((e_opt_number_of_desks - 1) * 3);
  for (i = (gint) e_opt_number_of_desks - 1; i >= 0; i--)
    {
      x = 2 + (i * 3);
      y = 2 + (i * 3);
      gtk_draw_box(s, win, GTK_STATE_ACTIVE, GTK_SHADOW_OUT, x, y, w, h);
    }
  if (e_opt_number_of_desks < 2.0)
    g_snprintf(str, sizeof(str), _("%1.0f Desktop"), e_opt_number_of_desks);
  else
    g_snprintf(str, sizeof(str), _("%1.0f Desktops"), e_opt_number_of_desks);
  gdk_text_extents(s->font, str, strlen(str), &dummy, &dummy, &tw,
		   &ascent, &descent);
  x = 2 + ((ww - tw) / 2);
  y = 2 + ((hh - (ascent + descent)) / 2) + ascent;
  gtk_draw_box(s, win, GTK_STATE_NORMAL, GTK_SHADOW_IN,
	       x - 3, y - 3 - ascent, tw + 6, ascent + descent + 6);
  gtk_draw_string(s, win, GTK_STATE_NORMAL, x, y, str);
}

static void
e_cb_area_range(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c, *w, *label;
  GtkAdjustment      *adj;
  gfloat             *value;
  gchar               str[1024];

  w = GTK_WIDGET(data);
  adj = GTK_ADJUSTMENT(widget);
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(adj), "capplet");
  if (!in_init)
    e_changed((c), FALSE);
  value = (gfloat *) gtk_object_get_data(GTK_OBJECT(adj), "value");
  *value = adj->value;
  label = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(w), "label");
  g_snprintf(str, sizeof(str),
	     _("The virtual desktop size will be\n"
	       "%1.0f x %1.0f\n"
	       "screens in size"),
	     e_opt_area_width, e_opt_area_height);
  gtk_label_set_text(GTK_LABEL(label), str);
  e_cb_area_redraw(w, NULL);
}

static GtkWidget   *
e_areas_config(void)
{
  GtkWidget          *table, *range, *frame, *area, *label, *w;
  GtkObject          *adj;
  gchar               str[1024];

  table = gtk_table_new(1, 1, FALSE);
  gtk_widget_show(table);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
  gtk_widget_show(frame);
  gtk_table_attach_defaults(GTK_TABLE(table), frame, 1, 10, 1, 10);

  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_drawing_area_size(GTK_DRAWING_AREA(area), 200, 160);
  gtk_widget_set_usize(area, 200, 160);
  gtk_container_add(GTK_CONTAINER(frame), area);
  gtk_signal_connect(GTK_OBJECT(area), "expose_event",
		     GTK_SIGNAL_FUNC(e_cb_area_redraw), NULL);

  adj = gtk_adjustment_new(e_opt_area_width, 1.0, 9.0, 1.0, 1.0, 1.0);
  range = gtk_hscale_new(GTK_ADJUSTMENT(adj));
  gtk_widget_set_usize(range, 200, -1);
  gtk_object_set_data(GTK_OBJECT(adj), "capplet", current_capplet);
  gtk_object_set_data(GTK_OBJECT(adj), "value", &e_opt_area_width);
  gtk_scale_set_digits(GTK_SCALE(range), 0);
  gtk_widget_show(range);
  gtk_table_attach_defaults(GTK_TABLE(table), range, 1, 10, 0, 1);
  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		     GTK_SIGNAL_FUNC(e_cb_area_range), (gpointer) area);

  adj = gtk_adjustment_new(e_opt_area_height, 1.0, 9.0, 1.0, 1.0, 1.0);
  range = gtk_vscale_new(GTK_ADJUSTMENT(adj));
  gtk_widget_set_usize(range, -1, 150);
  gtk_object_set_data(GTK_OBJECT(adj), "capplet", current_capplet);
  gtk_object_set_data(GTK_OBJECT(adj), "value", &e_opt_area_height);
  gtk_scale_set_digits(GTK_SCALE(range), 0);
  gtk_widget_show(range);
  gtk_table_attach_defaults(GTK_TABLE(table), range, 0, 1, 1, 10);
  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		     GTK_SIGNAL_FUNC(e_cb_area_range), (gpointer) area);
  g_snprintf(str, sizeof(str),
	     _("The virtual desktop size will be\n"
	       "%1.0f x %1.0f\n"
	       "screens in size"),
	     e_opt_area_width, e_opt_area_height);
  label = gtk_label_new(str);
  gtk_widget_show(label);
  gtk_table_attach_defaults(GTK_TABLE(table), label, 1, 10, 10, 11);

  w = e_create_rangeonoff(_("Edge flip resistance (0.01 sec)"),
			  &e_opt_edge_flip_resistance,
			  1.0, 100.0,
			  _("-"), _("+"),
			  NULL, 0.0);
  gtk_table_attach_defaults(GTK_TABLE(table), w, 1, 10, 11, 12);

  gtk_object_set_data(GTK_OBJECT(area), "label", label);
  gtk_object_set_data(GTK_OBJECT(area), "table", table);
  return table;
}

static void
e_cb_desk_range(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c, *w;
  GtkAdjustment      *adj;
  gfloat             *value;

  w = GTK_WIDGET(data);
  adj = GTK_ADJUSTMENT(widget);
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(adj), "capplet");
  if (!in_init)
    e_changed((c), FALSE);
  value = (gfloat *) gtk_object_get_data(GTK_OBJECT(adj), "value");
  *value = adj->value;
  e_cb_desk_redraw(w, NULL);
  e_rebuild_desk_menu();
}

static GtkWidget   *
e_desktops_config(void)
{
  GtkWidget          *table, *range, *frame, *area, *label, *window;
  GtkObject          *adj;

  table = gtk_table_new(1, 1, FALSE);
  gtk_widget_show(table);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
  gtk_widget_show(frame);
  gtk_table_attach_defaults(GTK_TABLE(table), frame, 1, 10, 1, 10);

  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_drawing_area_size(GTK_DRAWING_AREA(area), 153, 138);
  gtk_widget_set_usize(area, 153, 138);
  gtk_container_add(GTK_CONTAINER(frame), area);
  gtk_signal_connect(GTK_OBJECT(area), "expose_event",
		     GTK_SIGNAL_FUNC(e_cb_desk_redraw), NULL);

  adj = gtk_adjustment_new(e_opt_number_of_desks, 1.0, 33.0, 1.0, 1.0, 1.0);
  range = gtk_vscale_new(GTK_ADJUSTMENT(adj));
  gtk_widget_set_usize(range, -1, 138);
  gtk_object_set_data(GTK_OBJECT(adj), "capplet", current_capplet);
  gtk_object_set_data(GTK_OBJECT(adj), "value", &e_opt_number_of_desks);
  gtk_scale_set_digits(GTK_SCALE(range), 0);
  gtk_widget_show(range);
  gtk_table_attach_defaults(GTK_TABLE(table), range, 0, 1, 1, 10);
  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
		     GTK_SIGNAL_FUNC(e_cb_desk_range), (gpointer) area);
  label = gtk_label_new(_("The number of separate desktops\n"
			  "layered on top of eachother"));
  gtk_widget_show(label);
  gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 10, 10, 11);

  return table;
}

static void
e_cb_reset(GtkWidget * widget, gpointer data)
{
  CommsSend("set_controls AUTOSAVE: 0");
  CommsSend("save_config");
  CommsSend("set_default_theme DEFAULT");
  CommsSend("restart");
  gdk_flush();
  exit(0);
}

/* Pane for focus & display */
static GtkWidget   *
__setup_pane_1(void)
{
  GtkWidget          *align, *vbox, *w;
  IconOption          move_options[] =
    {
	{N_("Opaque"), move_opaque_xpm,
	  N_("Windows are moved in a solid fashion")},
	{N_("Lined"), move_lined_xpm,
	  N_("Windows are moved by drawing construction lines around them")},
	{N_("Box"), move_box_xpm,
	  N_("Windows are moved by drawing a box as their outline")},
	{N_("Shaded"), move_shaded_xpm,
	  N_("Windows are moved by drawing a stippled outline of them")},
	{N_("Semi-Solid"), move_semi_solid_xpm,
	  N_("Windows are moved by drawing a chequered outline of them")},
	{N_("Translucent"), move_translucent_xpm,
	  N_("Windows are moved by drawing a translucent copy of the window")}
    };
  IconOption          resize_options[] =
    {
	{N_("Opaque"), resize_opaque_xpm,
	  N_("Windows are resized as you drag")},
	{N_("Lined"), resize_lined_xpm,
	  N_("Windows get resized by displaying construction lines around them")},
	{N_("Box"), resize_box_xpm,
	  N_("Windows are resized by drawing a box as their outline")},
	{N_("Shaded"), resize_shaded_xpm,
	  N_("windows are resized by drawing a stippled outline of them")},
	{N_("Semi-Solid"), resize_semi_solid_xpm,
	  N_("Windows are resized by drawing a chequered outline of them")}
    };
  IconOption          focus_options[] =
    {
	{N_("Mouse Pointer"), focus_pointer_xpm,
	  N_("Your keypresses are sent to whatever window your mouse is over at the time")},
	{N_("Sloppy Pointer"), focus_sloppy_xpm,
	  N_("Your keypresses are sent to the window your mouse was over last, if any")},
	{N_("Pointer Clicks"), focus_click_xpm,
	  N_("You keypresses are sent to the window you last clicked on")},
    };
  
  vbox = gtk_vbox_new(FALSE, 1);
  
  w = e_create_icon_option(_("Move Methods"), &e_opt_move,
			   6, 6, move_options);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_icon_option(_("Resize Methods"), &e_opt_resize,
			   6, 5, resize_options);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_icon_option(_("Keyboard focus follows"), &e_opt_focus,
			   6, 3, focus_options);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  align = gtk_alignment_new(0.5, 0.5, 0.0, 0.0);
  gtk_widget_show(align);
  w = gtk_button_new_with_label(_("Reset all settings to system defaults and exit"));
  gtk_signal_connect(GTK_OBJECT(w), "clicked",
		     GTK_SIGNAL_FUNC(e_cb_reset), NULL);
  gtk_widget_show(w);
  gtk_container_add(GTK_CONTAINER(align), w);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 1);
  gtk_widget_show(vbox);
  return vbox;
}

static void
e_cb_ebg(GtkWidget *widget, gpointer data)
{
  if (GTK_TOGGLE_BUTTON(widget)->active)
    {
      gtk_widget_set_sensitive(bg_widget, TRUE);
      gtk_clist_set_selectable(GTK_CLIST(section_clist), 4, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive(bg_widget, FALSE);
      gtk_clist_set_selectable(GTK_CLIST(section_clist), 4, FALSE);
    }
}

static void
e_cb_dbar(GtkWidget *widget, gpointer data)
{
  GtkWidget *c;
  
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
  if (GTK_TOGGLE_BUTTON(widget)->active)
    e_opt_dragbarwidth = 16;
  else
    e_opt_dragbarwidth = 0;
  e_changed(c, FALSE);
}

/* Pane for special effects */
static GtkWidget   *
__setup_pane_2(void)
{
  GtkWidget          *frame, *frame2, *hbox, *vbox, *w, *tip, *vbox2;
  IconOption          slide_options[] =
    {
	{N_("Opaque"), move_opaque_xpm,
	  N_("Windows are slid in a solid fashion")},
	{N_("Lined"), move_lined_xpm,
	  N_("Windows are slid by drawing construction lines around them")},
	{N_("Box"), move_box_xpm,
	  N_("Windows are slid by drawing a box as their outline")},
	{N_("Shaded"), move_shaded_xpm,
	  N_("Windows are slid by drawing a stippled outline of them")},
	{N_("Semi-Solid"), move_semi_solid_xpm,
	  N_("Windows are slid by drawing a chequered outline of them")},
    };

  hbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox);
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 1);
  w = e_create_icon_option(_("Window Sliding Methods"), &e_opt_slide_mode,
			   6, 5, slide_options);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_rangeonoff(_("Windows slide in when they appear"),
			  &e_opt_map_speed,
			  16.0, 20000.0,
			  _("-"), _("+"),
			  &e_opt_slide_map, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_rangeonoff(_("Windows slide about during window cleanup"),
			  &e_opt_cleanup_speed,
			  16.0, 20000.0,
			  _("-"), _("+"),
			  &e_opt_slide_cleanup, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_rangeonoff(_("Desktops slide in when changing desktops"),
			  &e_opt_desk_speed,
			  16.0, 20000.0,
			  _("-"), _("+"),
			  &e_opt_slide_desk, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_rangeonoff(_("Window shading speed (pixels / sec)"),
			  &e_opt_shade_speed,
			  16.0, 20000.0,
			  _("-"), _("+"),
			  NULL, 99999.0);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  if (is_capp)
    {
      w = gtk_check_button_new_with_label(_("Enable Enlightenments background selection"));
      gtk_widget_show(w);
      tip = (GtkWidget *) gtk_tooltips_new();
      gtk_tooltips_set_tip(GTK_TOOLTIPS(tip), w, 
			   _("This enables Enlightenments background selector. "
			     "If backgrounds are selected in this section "
			     "any backgrounds selected in any other selector "
			     "or set by any other programs on desktop 0 "
			     "will be overidden by Enlightenment"), NULL);
      gtk_tooltips_enable(GTK_TOOLTIPS(tip));
      gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
      gtk_signal_connect(GTK_OBJECT(w), "toggled",
			 GTK_SIGNAL_FUNC(e_cb_ebg), NULL);
    }
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 1);
    {
      GtkWidget *check;
      
      frame = gtk_frame_new(_("Drag bar"));
      gtk_widget_show(frame);
      gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 1);
      vbox2 = gtk_vbox_new(FALSE, 1);
      gtk_widget_show(vbox2);
      gtk_container_add(GTK_CONTAINER(frame), vbox2);
      check = gtk_check_button_new_with_label(_("Enable"));
      gtk_object_set_data(GTK_OBJECT(check), "capplet", current_capplet);
      if (e_opt_dragbarwidth > 0)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 1);
      else
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 0);      
      gtk_widget_show(check);
      gtk_box_pack_start(GTK_BOX(vbox2), check, FALSE, FALSE, 1);
      gtk_signal_connect(GTK_OBJECT(check), "toggled",
			 GTK_SIGNAL_FUNC(e_cb_dbar), NULL);
      w = e_add_multi_to_frame(_("Location:"),
			       _("Left\nRight\nTop\nBottom"),
			       &e_opt_dragdir, 0);
      gtk_box_pack_start(GTK_BOX(vbox2), w, FALSE, FALSE, 1);
    }
  w = e_create_onoff(_("Animate menus"), &e_opt_menuslide);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Reduce refresh\n(use SaveUnders)"),
		     &e_opt_saveunders);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  return hbox;
}

/* Pane for behavior */
static GtkWidget   *
__setup_pane_3(void)
{
  GtkWidget          *vbox, *w, *note, *label;

  note = gtk_notebook_new();
  gtk_widget_show(note);
  
  /*******************************************************************/
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  label = gtk_label_new(_("Advanced Focus"));
  gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(note), vbox, label);
  w = e_create_onoff(_("All new windows that appear get the keyboard focus"),
		     &e_opt_all_new_windows_get_focus);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("All new popup windows get the keyboard focus"),
		     &e_opt_new_transients_get_focus);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Only new popup windows whose owner is focused get the keyboard focus"),
		     &e_opt_e_opt_new_transients_get_focus_if_group_focused);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Raise windows when switching focus with the keyboard"),
		     &e_opt_raise_on_focus);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Send the pointer to windows when switching focus with the keyboard"),
		     &e_opt_warp_on_focus);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  
  /*******************************************************************/
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  label = gtk_label_new(_("Miscellaneous"));
  gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(note), vbox, label);  
  w = e_create_rangeonoff(_("Tooltips ON/OFF & timeout for tooltip popup (sec)"),
			  &e_opt_tooltiptime,
			  0.0, 10.0,
			  _("-"), _("+"),
			  &e_opt_tooltips, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_rangeonoff(_("Automatic raising of windows after X seconds"),
			  &e_opt_autoraisetime,
			  0.0, 10.0,
			  _("-"), _("+"),
			  &e_opt_autorase, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Transient popup windows appear together with leader"),
		     &e_opt_transients_follow_leader);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Switch to where popup window appears"),
		     &e_opt_switch_for_transient_map);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Display icons when windows are iconified"),
		     &e_opt_show_icons);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  w = e_create_onoff(_("Place windows manually"),
		     &e_opt_manual_placement);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  
  return note;
}

/* Pane for desktops */
static GtkWidget   *
__setup_pane_4(void)
{
  GtkWidget          *hbox, *vbox, *w, *frame, *align;

  hbox = gtk_hbox_new(FALSE, 1);

  frame = gtk_frame_new(_("Size of Virtual Screen"));
  gtk_widget_show(frame);
  align = gtk_alignment_new(0.5, 0.0, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(frame), align);
  gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 1);
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(align), vbox);

  w = e_areas_config();
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  frame = gtk_frame_new(_("Separate Desktops"));
  gtk_widget_show(frame);
  align = gtk_alignment_new(0.5, 0.0, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(frame), align);
  gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 1);
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(align), vbox);

  w = e_desktops_config();
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  gtk_widget_show(hbox);
  return hbox;
}

/* Pane for backgrounds */

static void
e_set_bg_to_e(Background *bg)
{
  gchar               buf[4096], buf2[4096];
  gint                i;

  if (!bg)
    return;
  g_snprintf(buf, sizeof(buf), "set_bg_colmod %s (NULL)", bg->name);
  if (bg->cm)
    g_snprintf(buf, sizeof(buf), "set_bg_colmod %s %s", bg->name, bg->cm->name);
  CommsSend(buf);
  if (bg->cm)
    {
      g_snprintf(buf, sizeof(buf), "set_colmod %s %i",bg->cm->name, 
		 (gint)(bg->cm->red.num));
      for (i = 0; i < bg->cm->red.num; i++)
	{
	  g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->red.px[i]));
	  strcat(buf, buf2);
	  g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->red.py[i]));
	  strcat(buf, buf2);
	}
      g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->green.num));
      strcat(buf, buf2);
      for (i = 0; i < bg->cm->green.num; i++)
	{
	  g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->green.px[i]));
	  strcat(buf, buf2);
	  g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->green.py[i]));
	  strcat(buf, buf2);
	}
      g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->red.num));
      strcat(buf, buf2);
      for (i = 0; i < bg->cm->blue.num; i++)
	{
	  g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->blue.px[i]));
	  strcat(buf, buf2);
	  g_snprintf(buf2, sizeof(buf2), " %i", (gint)(bg->cm->blue.py[i]));
	  strcat(buf, buf2);
	}
      CommsSend(buf);
    }
  if ((bg->bg.file) && (bg->top.file))
    g_snprintf(buf, sizeof(buf),
	       "set_bg %s %i %i %i %s %i %i %i %i %i %i %s %i %i %i %i %i",
	       bg->name,
	       bg->bg.solid.r, bg->bg.solid.g, bg->bg.solid.b,
	       bg->bg.file, bg->bg.tile, bg->bg.keep_aspect,
	       bg->bg.xjust, bg->bg.yjust, bg->bg.xperc, bg->bg.yperc,
	       bg->top.file, bg->top.keep_aspect, bg->top.xjust,
	       bg->top.yjust, bg->top.xperc, bg->top.yperc);
  else if ((!(bg->bg.file)) && (bg->top.file))
    g_snprintf(buf, sizeof(buf),
	       "set_bg %s %i %i %i %s %i %i %i %i %i %i %s %i %i %i %i %i",
	       bg->name,
	       bg->bg.solid.r, bg->bg.solid.g, bg->bg.solid.b,
	       "(null)", bg->bg.tile, bg->bg.keep_aspect,
	       bg->bg.xjust, bg->bg.yjust, bg->bg.xperc, bg->bg.yperc,
	       bg->top.file, bg->top.keep_aspect, bg->top.xjust,
	       bg->top.yjust, bg->top.xperc, bg->top.yperc);
  else if ((bg->bg.file) && (!(bg->top.file)))
    g_snprintf(buf, sizeof(buf),
	       "set_bg %s %i %i %i %s %i %i %i %i %i %i %s %i %i %i %i %i",
	       bg->name,
	       bg->bg.solid.r, bg->bg.solid.g, bg->bg.solid.b,
	       bg->bg.file, bg->bg.tile, bg->bg.keep_aspect,
	       bg->bg.xjust, bg->bg.yjust, bg->bg.xperc, bg->bg.yperc,
	       "(null)", bg->top.keep_aspect, bg->top.xjust,
	       bg->top.yjust, bg->top.xperc, bg->top.yperc);
  else if ((!(bg->bg.file)) && (!(bg->top.file)))
    g_snprintf(buf, sizeof(buf),
	       "set_bg %s %i %i %i %s %i %i %i %i %i %i %s %i %i %i %i %i",
	       bg->name,
	       bg->bg.solid.r, bg->bg.solid.g, bg->bg.solid.b,
	       "(null)", bg->bg.tile, bg->bg.keep_aspect,
	       bg->bg.xjust, bg->bg.yjust, bg->bg.xperc, bg->bg.yperc,
	       "(null)", bg->top.keep_aspect, bg->top.xjust,
	       bg->top.yjust, bg->top.xperc, bg->top.yperc);
  CommsSend(buf);
}

static void 
e_set_bg_setting_widgets(void)
{
  gint a, r, g, b;
  gdouble             col[4];
  
  gtk_signal_handler_block(GTK_OBJECT(bg_bg_tile), h1);
  gtk_signal_handler_block(GTK_OBJECT(bg_bg_aspect), h2);
  gtk_signal_handler_block(GTK_OBJECT(bg_bg_maxv), h3);
  gtk_signal_handler_block(GTK_OBJECT(bg_bg_maxh), h4);
  gtk_signal_handler_block(GTK_OBJECT(bg_fg_aspect), h5);
  gtk_signal_handler_block(GTK_OBJECT(bg_fg_maxv), h6);
  gtk_signal_handler_block(GTK_OBJECT(bg_fg_maxh), h7);
/*  gtk_signal_handler_block(GTK_OBJECT(color_selector), h8);*/
/*  gtk_signal_handler_block(GTK_OBJECT(bg_fg_pos), h9);
  gtk_signal_handler_block(GTK_OBJECT(bg_bg_file_entry), h10);
 */
  if (deskbg[curdesk])
    {
      if (deskbg[curdesk]->bg.file)
	gtk_entry_set_text(GTK_ENTRY(bg_bg_file_entry), 
			   deskbg[curdesk]->bg.file);
      else
	gtk_entry_set_text(GTK_ENTRY(bg_bg_file_entry), "");
      if (deskbg[curdesk]->top.file)
	gtk_entry_set_text(GTK_ENTRY(bg_fg_file_entry), 
			   deskbg[curdesk]->top.file);
      else
	gtk_entry_set_text(GTK_ENTRY(bg_fg_file_entry), "");
      
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_tile), 
				  deskbg[curdesk]->bg.tile);

      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_aspect), 
				  deskbg[curdesk]->bg.keep_aspect);
      
      if (deskbg[curdesk]->bg.yperc > 512)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_maxv), 1);
      else
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_maxv), 0);
      
      if (deskbg[curdesk]->bg.xperc > 512)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_maxh), 1);
      else
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_maxh), 0);
      
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_aspect), 
				  deskbg[curdesk]->top.keep_aspect);
      
      if (deskbg[curdesk]->top.yperc > 512)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_maxv), 1);
      else
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_maxv), 0);
      
      if (deskbg[curdesk]->top.xperc > 512)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_maxh), 1);
      else
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_maxh), 0);

      a = deskbg[curdesk]->top.xjust / 341;
      b = deskbg[curdesk]->top.yjust / 341;
      if (a < 0) 
	a = 0;
      else if (a > 2)
	a = 2;
      if (b < 0) 
	b = 0;
      else if (b > 2)
	b = 2;
      switch((b * 3) + a)
	{
	 case 0:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 0);
	  break;
	 case 1:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 1);
	  break;
	 case 2:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 2);
	  break;
	 case 3:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 7);
	  break;
	 case 4:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 8);
	  break;
	 case 5:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 3);
	  break;
	 case 6:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 6);
	  break;
	 case 7:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 5);
	  break;
	 case 8:
	  gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 4);
	  break;
	 default:
	  break;
	}
      
      if (color_selector)
	{      
	  r = deskbg[curdesk]->bg.solid.r;
	  g = deskbg[curdesk]->bg.solid.g;
	  b = deskbg[curdesk]->bg.solid.b;
	  col[0] = ((gdouble) r) / 255;
	  col[1] = ((gdouble) g) / 255;
	  col[2] = ((gdouble) b) / 255;
	  col[3] = 0;
	  gtk_color_selection_set_color(GTK_COLOR_SELECTION(color_selector), 
					col);
	}
    }
  else
    {
      gtk_entry_set_text(GTK_ENTRY(bg_bg_file_entry), "");
      gtk_entry_set_text(GTK_ENTRY(bg_fg_file_entry), "");
      
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_tile), 0);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_aspect), 0);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_maxv), 0);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_bg_maxh), 0);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_aspect), 0);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_maxv), 0);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(bg_fg_maxh), 0);
      gtk_option_menu_set_history(GTK_OPTION_MENU(bg_fg_pos), 0);
      
      if (color_selector)
	{      
	  r = 0;
	  g = 0;
	  b = 0;
	  col[0] = ((gdouble) r) / 255;
	  col[1] = ((gdouble) g) / 255;
	  col[2] = ((gdouble) b) / 255;
	  col[3] = 0;
	  gtk_color_selection_set_color(GTK_COLOR_SELECTION(color_selector), 
					col);
	}
    }
  gtk_signal_handler_unblock(GTK_OBJECT(bg_bg_tile), h1);
  gtk_signal_handler_unblock(GTK_OBJECT(bg_bg_aspect), h2);
  gtk_signal_handler_unblock(GTK_OBJECT(bg_bg_maxv), h3);
  gtk_signal_handler_unblock(GTK_OBJECT(bg_bg_maxh), h4);
  gtk_signal_handler_unblock(GTK_OBJECT(bg_fg_aspect), h5);
  gtk_signal_handler_unblock(GTK_OBJECT(bg_fg_maxv), h6);
  gtk_signal_handler_unblock(GTK_OBJECT(bg_fg_maxh), h7);
/*  gtk_signal_handler_unblock(GTK_OBJECT(color_selector), h8);*/
/*  gtk_signal_handler_unblock(GTK_OBJECT(bg_fg_pos), h9);
  gtk_signal_handler_unblock(GTK_OBJECT(bg_bg_file_entry), h10);
 */
}

static void
e_display_desktop_bg(void)
{
  static GdkPixmap *pmap = NULL;
  Background *bg = NULL;
  GtkWidget *area;
  GdkColor col;
  
  area = desk_mini_display_area;
  if (!area)
    return;
  if (!GTK_WIDGET_REALIZED(area))
    return;
  if (deskbg[curdesk])
    {
      if (!pmap)
	pmap = gdk_pixmap_new(area->window, 128, 96, -1);
      gdk_window_set_back_pixmap(area->window, pmap, FALSE);
      gdk_flush();
      e_render_bg_onto(pmap, deskbg[curdesk]);
      gdk_flush();
      gdk_window_clear(area->window);
    }
  else
    {
      gdk_color_black(gdk_colormap_get_system(), &col);
      gdk_window_set_background(area->window, &col);
      gdk_window_clear(area->window);
    }
}

static void
e_cb_expose_mini_desk(GtkWidget *widget, gpointer data)
{
  static gchar did_first_draw = 0;
  
  if (!did_first_draw)
    {
      did_first_draw = 1;
      e_display_desktop_bg();
    }
}

static void
e_cb_expose_crange(GtkWidget *widget, gpointer data)
{
  static gchar did_first_draw = 0;
  
  if (!did_first_draw)
    {
      e_set_bg_setting_widgets();
      did_first_draw = 1;
    }
}

static void
e_cb_set_current_desk(GtkWidget *widget, gpointer data)
{
  curdesk = (gint) data;
  e_display_desktop_bg();
  e_set_bg_setting_widgets();
}

static void
e_rebuild_desk_menu(void)
{
  gint i;
  GtkWidget *mi;
  gchar               s[256];

  if (desk_select_menu)
    gtk_widget_destroy(desk_select_menu);
  desk_select_menu = gtk_menu_new();
  gtk_widget_show(desk_select_menu);
  for (i = 0; i < (gint) e_opt_number_of_desks; i++)
    {
      g_snprintf(s, sizeof(s), _("Desktop %i"), i);
      mi = gtk_menu_item_new_with_label(s);
      gtk_widget_show(mi);
      gtk_signal_connect(GTK_OBJECT(mi), "activate", 
			 GTK_SIGNAL_FUNC(e_cb_set_current_desk), (gpointer)i);
      gtk_menu_append(GTK_MENU(desk_select_menu), mi);
    }
  if (desk_option_menu)
    {
      gtk_option_menu_set_menu(GTK_OPTION_MENU(desk_option_menu), desk_select_menu);
      gtk_option_menu_set_history(GTK_OPTION_MENU(desk_option_menu), 0);
      if (curdesk != 0)
	{
	  curdesk = 0;      
	  e_display_desktop_bg();
	  e_set_bg_setting_widgets();
	}
    }
}

static void
e_cb_bg_sel(GtkWidget *widget, gpointer data)
{
  Background *bg;
  GtkWidget *c;
  
  bg = (Background *)gtk_object_get_data(GTK_OBJECT(widget), "bg");  
  if (!bg)
    return;
  if (bg != deskbg[curdesk])
    {
      deskbgchanged[curdesk] = 1;
      c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      if (!in_init)
	e_changed((c), FALSE);
      deskbg[curdesk] = bg;
      e_display_desktop_bg();
      e_set_bg_setting_widgets();
    }
 }

static void
e_cb_grad_sel(GtkWidget *widget, gpointer data)
{
  Background *bg;
  GtkWidget *c;
  
  bg = (Background *)data;  
  if (!bg)
    return;
  if (bg != deskbg[curdesk])
    {
      deskbgchanged[curdesk] = 1;
      c = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      if (!in_init)
	e_changed((c), FALSE);
      deskbg[curdesk] = bg;
      e_display_desktop_bg();
      e_set_bg_setting_widgets();
    }
 }

static void
e_cb_bg_none(GtkWidget *widget, gpointer data)
{
  deskbgchanged[curdesk] = 1;
  deskbg[curdesk] = NULL;
  
  e_set_bg_setting_widgets();
  e_display_desktop_bg();
  if (!in_init)
    e_changed((current_capplet), FALSE);
}

static void
e_cb_bg_new(GtkWidget *widget, gpointer data)
{
  GdkPixmap *pmap;
  GdkImlibImage *im;
  Background *bg;
  gint j, la, ra, ta, ba;
  gchar s[4096];
  
  j = g_list_length(backgrounds);
  
  bg = g_malloc(sizeof(Background));
  
  backgrounds = g_list_append(backgrounds, bg);
  
  g_snprintf(s, sizeof(s), "ECONF_GEN_%x_%x", time(NULL), rand());
  bg->name = g_strdup(s);
  bg->bg.file = NULL;
  bg->top.file = NULL;
  bg->gpmap = NULL;
  bg->bg.solid.r = 0;
  bg->bg.solid.g = 0;
  bg->bg.solid.b = 0;
  bg->bg.tile = 1;
  bg->bg.keep_aspect = 0;
  bg->bg.xjust = 512;
  bg->bg.yjust = 512;
  bg->bg.xperc = 0;
  bg->bg.yperc = 0;
  bg->top.keep_aspect = 0;
  bg->top.xjust = 512;
  bg->top.yjust = 512;
  bg->top.xperc = 0;
  bg->top.yperc = 0;
  bg->changed = 0;
  bg->cm = NULL;
  e_set_bg_to_e(bg);
  
  deskbg[curdesk] = bg;
  g_snprintf(s, sizeof(s), "%s/.e-conf/%s", getenv("HOME"), bg->name);

  pmap = gdk_pixmap_new(current_capplet->window, 64, 48, -1);
  e_render_bg_onto(pmap, bg);
  im = gdk_imlib_create_image_from_drawable(pmap, NULL, 0, 0, 64, 48);
  if (im)
    {
      gdk_imlib_save_image_to_ppm(im, s);
      gdk_imlib_kill_image(im);
    }

  bg->pmap = gtk_pixmap_new(pmap, NULL);
  bg->gpmap = NULL;
  bg->table = image_table_list;
  gdk_pixmap_unref(pmap);

  bg->button = gtk_button_new();
  gtk_object_set_data(GTK_OBJECT(bg->button), "bg", bg);
  gtk_object_set_data(GTK_OBJECT(bg->button), "capplet", 
		      (gpointer) current_capplet);
  gtk_widget_show(bg->button);
  bg->box = gtk_hbox_new(FALSE, 1);
  gtk_container_set_border_width(GTK_CONTAINER(bg->box), 1);
  gtk_widget_show(bg->box);
  gtk_container_add(GTK_CONTAINER(bg->button), bg->box);
  gtk_widget_show(bg->pmap);
  gtk_box_pack_start(GTK_BOX(bg->box), bg->pmap, FALSE, FALSE, 0);
  gtk_signal_connect(GTK_OBJECT(bg->button), "clicked",
		     GTK_SIGNAL_FUNC(e_cb_bg_sel), NULL);
  ta = j / table_size;
  ba = ta + 1;
  la = j % table_size;
  ra = la + 1;
  gtk_table_attach_defaults(GTK_TABLE(image_table_list), bg->button, 
			    la, ra, ta, ba);
  
  deskbgchanged[curdesk] = 1;
  deskbg[curdesk] = bg;  
  e_set_bg_setting_widgets();
  e_display_desktop_bg();
  if (!in_init)
    e_changed((current_capplet), FALSE);
  
  gtk_widget_show(edit_win);
}

static void
e_del_bg_from_e(Background *bg)
{
  gchar buf[256], cmd[4096];
  gchar changed = 0;
  gint i;
  
  g_snprintf(cmd, sizeof(cmd), "use_no_bg ");
  for (i = 0; i < 32; i++)
    {
      if (deskbg[i] == bg)
	{
	  g_snprintf(buf, sizeof(buf), " %i", i);
	  strcat(cmd, buf);
	  changed = 1;
	  deskbg[i] = NULL;
	}
    }
  if (changed)
    CommsSend(cmd);
  g_snprintf(cmd, sizeof(cmd), "del_bg %s", bg->name);
  CommsSend(cmd);
}

static void
e_cb_bg_delete(GtkWidget *widget, gpointer data)
{
  deskbgchanged[curdesk] = 1;

  if ((deskbg[curdesk]) && (deskbg[curdesk]->gpmap))
    return;
  if (backgrounds)
    {
      gchar               s[4096], have_deleted = 0;
      int                 i, j, la, ra, ta, ba;
      GList              *ptr;

      j = g_list_length(backgrounds);

      ptr = backgrounds;
      i = 0;
      while(ptr)
	{
	  Background         *bg = NULL;
	  GtkWidget          *l_hb, *l_label, *l_pixmap, *l_item;

	  if (progress)
	    {
	      gtk_progress_bar_update(GTK_PROGRESS_BAR(progress),
				      (gfloat) i / (gfloat) j);
	      gtk_widget_draw(progress, NULL);
	    }
	  bg = ptr->data;
	  ptr = ptr->next;
	  if (bg)
	    {
	      if (bg == deskbg[curdesk])
		{
		  e_del_bg_from_e(bg);
		  backgrounds = g_list_remove(backgrounds, bg);
		  gtk_widget_destroy(bg->button);
		  if (bg->name)
		    g_free(bg->name);
		  if (bg->bg.file)
		    g_free(bg->bg.file);
		  if (bg->top.file)
		    g_free(bg->top.file);
		  g_free(bg);
		  have_deleted = 1;
		  i--;
		}
	      else if (have_deleted)
		{
		  ta = i / table_size;
		  ba = ta + 1;
		  la = i % table_size;
		  ra = la + 1;
		  gtk_widget_ref(bg->button);
		  gtk_container_remove(GTK_CONTAINER(bg->table), bg->button);
		  gtk_table_attach_defaults(GTK_TABLE(bg->table), bg->button, 
					    la, ra, ta, ba);
		  gtk_widget_unref(bg->button);
		}
	    }
	  i++;
	}
    }
  
  deskbg[curdesk] = NULL;
  e_set_bg_setting_widgets();
  e_display_desktop_bg();
  if (!in_init)
    e_changed((current_capplet), FALSE);
}

static void
e_cb_bg_apply(GtkWidget *widget, gpointer data)
{
  deskbgchanged[curdesk] = 1;
  if (deskbg[curdesk])
    {
      deskbg[curdesk]->changed = 1;
      e_set_bg_to_e(deskbg[curdesk]);
      redo_bg(deskbg[curdesk]);
    }
  e_set_bg_setting_widgets();
  e_display_desktop_bg();
  if (!in_init)
    e_changed((current_capplet), FALSE);
}

static void
redo_bg(Background *bg)
{
  GtkWidget          *l_pixmap;
  GdkPixmap          *pmap;
  GdkImlibImage      *im;
  gchar               s[4096];
  
  if (!bg)
    return;
  
  pmap = gdk_pixmap_new(current_capplet->window, 64, 48, -1);
  g_snprintf(s, sizeof(s), "%s/.e-conf/%s", getenv("HOME"), bg->name);
  im = gdk_imlib_load_image(s);
  e_render_bg_onto(pmap, bg);
  im = gdk_imlib_create_image_from_drawable(pmap, NULL, 0, 0,
					    64, 48);
  if (im)
    {
      gdk_imlib_save_image_to_ppm(im, s);
      gdk_imlib_kill_image(im);
    }
  l_pixmap = gtk_pixmap_new(pmap, NULL);
  gdk_pixmap_unref(pmap);
  gtk_widget_destroy(bg->pmap);
  bg->pmap = l_pixmap;
  gtk_widget_show(l_pixmap);
  gtk_box_pack_start(GTK_BOX(bg->box), l_pixmap, FALSE, FALSE, 0);
}

static void
e_cb_img_onoff(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c;
  Background         *bg;
  gint                value;
  
  bg = deskbg[curdesk];
  if (!bg)
    return;
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
  if (!in_init)
    e_changed((c), FALSE);
  value = (gint) data;
  
  if (GTK_TOGGLE_BUTTON(widget)->active)
    {
      switch(value)
	{
	 case 0:
	  if (bg->bg.tile != 1)
	    {
	      bg->bg.tile = 1;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 1:
	  if (bg->bg.yperc != 1024)
	    {
	      bg->bg.yperc = 1024;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 2:
	  if (bg->bg.xperc != 1024)
	    {
	      bg->bg.xperc = 1024;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 3:
	  if (bg->bg.keep_aspect != 1)
	    {
	      bg->bg.keep_aspect = 1;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 4:
	  if (bg->top.yperc != 1024)
	    {
	      bg->top.yperc = 1024;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 5:
	  if (bg->top.xperc != 1024)
	    {
	      bg->top.xperc = 1024;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 6:
	  if (bg->top.keep_aspect != 1)
	    {
	      bg->top.keep_aspect = 1;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 default:
	  break;
	}
    }
  else
    {
      switch(value)
	{
	 case 0:
	  if (bg->bg.tile != 0)
	    {
	      bg->bg.tile = 0;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 1:
	  if (bg->bg.yperc != 0)
	    {
	      bg->bg.yperc = 0;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 2:
	  if (bg->bg.xperc != 0)
	    {
	      bg->bg.xperc = 0;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 3:
	  if (bg->bg.keep_aspect != 0)
	    {
	      bg->bg.keep_aspect = 0;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 4:
	  if (bg->top.yperc != 0)
	    {
	      bg->top.yperc = 0;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 5:
	  if (bg->top.xperc != 0)
	    {
	      bg->top.xperc = 0;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 case 6:
	  if (bg->top.keep_aspect != 0)
	    {
	      bg->top.keep_aspect = 0;
	      e_cb_bg_apply(NULL, NULL);
	    }
	  break;
	 default:
	  break;
	}
    }
}

static void
e_cb_img_position(GtkWidget * widget, gpointer data)
{
  GtkWidget          *c;
  Background         *bg;
  gint                value;
  
  bg = deskbg[curdesk];
  if (!bg)
    return;
  c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
  if (!in_init)
    e_changed((c), FALSE);
  value = (gint) data;
  
  switch(value)
    {
     case 0:
      if ((bg->top.xjust != 0) || (bg->top.yjust != 0))
	{
	  bg->top.xjust = 0;
	  bg->top.yjust = 0;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 1:
      if ((bg->top.xjust != 512) || (bg->top.yjust != 0))
	{
	  bg->top.xjust = 512;
	  bg->top.yjust = 0;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 2:
      if ((bg->top.xjust != 1024) || (bg->top.yjust != 0))
	{
	  bg->top.xjust = 1024;
	  bg->top.yjust = 0;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 3:
      if ((bg->top.xjust != 1024) || (bg->top.yjust != 512))
	{
	  bg->top.xjust = 1024;
	  bg->top.yjust = 512;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 4:
      if ((bg->top.xjust != 1024) || (bg->top.yjust != 1024))
	{
	  bg->top.xjust = 1024;
	  bg->top.yjust = 1024;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 5:
      if ((bg->top.xjust != 512) || (bg->top.yjust != 1024))
	{
	  bg->top.xjust = 512;
	  bg->top.yjust = 1024;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 6:
      if ((bg->top.xjust != 1024) || (bg->top.yjust != 1024))
	{
	  bg->top.xjust = 0;
	  bg->top.yjust = 1024;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 7:
      if ((bg->top.xjust != 0) || (bg->top.yjust != 512))
	{
	  bg->top.xjust = 0;
	  bg->top.yjust = 512;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     case 8:
      if ((bg->top.xjust != 512) || (bg->top.yjust != 512))
	{
	  bg->top.xjust = 512;
	  bg->top.yjust = 512;
	  e_cb_bg_apply(NULL, NULL);
	}
      break;
     default:
      break;
    }
}

static void
e_cb_change_col(GtkWidget * widget, GtkWidget * w)
{
  gdouble             col[4];
  gint                r, g, b;
  static gint         pr = -1, pg = -1, pb = -1;

  gtk_color_selection_get_color(GTK_COLOR_SELECTION(w), col);
  r = (gint)(col[0] * (gdouble) 255);
  g = (gint)(col[1] * (gdouble) 255);
  b = (gint)(col[2] * (gdouble) 255);
  
  if ((deskbg[curdesk]) &&
      (deskbg[curdesk]->bg.solid.r == r) &&
      (deskbg[curdesk]->bg.solid.g == g) &&
      (deskbg[curdesk]->bg.solid.b == b))
    {
      return;
    }
  
  if (deskbg[curdesk])
    {
      deskbg[curdesk]->changed = 1;
      deskbg[curdesk]->bg.solid.r = r;
      deskbg[curdesk]->bg.solid.g = g;
      deskbg[curdesk]->bg.solid.b = b;
      if ((r == pr) && (g == pg) && (g = pg))
	return;
      e_cb_bg_apply(NULL, NULL);
    }
}

static GtkWidget *
e_create_solid_selector(void)
{
  GtkWidget *vbox, *colsel;
  
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  
  colsel = gtk_color_selection_new();
  gtk_color_selection_set_update_policy(GTK_COLOR_SELECTION(colsel),
					GTK_UPDATE_DISCONTINUOUS);
  gtk_widget_show(colsel);
  
  gtk_box_pack_start(GTK_BOX(vbox), colsel, FALSE, FALSE, 1);
  
  color_selector = colsel;
  h8 = gtk_signal_connect(GTK_OBJECT(colsel), "color_changed", 
			  GTK_SIGNAL_FUNC(e_cb_change_col), colsel);
  return vbox;
}

static GtkWidget *
e_create_gradient_editor(void)
{
  GtkWidget *vbox, *frame, *hbox, *radio, *area, *button, *colsel, *hbox2,
            *vbox2;
  GSList    *rlist = NULL;
  
  vbox = gtk_vbox_new(FALSE, 1);

  hbox2 = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, FALSE, 1);
  
  frame = gtk_frame_new(_("Number of colours in gradient"));
  gtk_widget_show(frame);
  gtk_box_pack_start(GTK_BOX(hbox2), frame, FALSE, FALSE, 1);
  
  hbox = gtk_hbox_new(TRUE, 1);
  gtk_widget_show(hbox);
  gtk_container_add(GTK_CONTAINER(frame), hbox);
  
  radio = gtk_radio_button_new_with_label(rlist, _("2"));
  gtk_widget_show(radio);
  rlist = g_slist_append(rlist, radio);
  gtk_box_pack_start(GTK_BOX(hbox), radio, TRUE, FALSE, 1);

  radio = gtk_radio_button_new_with_label(rlist, _("3"));
  gtk_widget_show(radio);
  rlist = g_slist_append(rlist, radio);
  gtk_box_pack_start(GTK_BOX(hbox), radio, TRUE, FALSE, 1);

  radio = gtk_radio_button_new_with_label(rlist, _("5"));
  gtk_widget_show(radio);
  rlist = g_slist_append(rlist, radio);
  gtk_box_pack_start(GTK_BOX(hbox), radio, TRUE, FALSE, 1);
  
  vbox2 = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox2);
  gtk_box_pack_start(GTK_BOX(hbox2), vbox2, TRUE, TRUE, 1);

  frame = gtk_frame_new(NULL);
  gtk_widget_show(frame);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
  gtk_box_pack_start(GTK_BOX(vbox2), frame, FALSE, FALSE, 1);
  
  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_widget_set_usize(area, -1, 16);
  gtk_container_add(GTK_CONTAINER(frame), area);    

  hbox = gtk_hbox_new(TRUE, 1);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox2), hbox, FALSE, FALSE, 1);
  
  button = gtk_button_new();
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, FALSE, 1);
  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_widget_set_usize(area, 16, 16);
  gtk_container_add(GTK_CONTAINER(button), area);    
  
  button = gtk_button_new();
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, FALSE, 1);
  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_widget_set_usize(area, 16, 16);
  gtk_container_add(GTK_CONTAINER(button), area);    
  
  button = gtk_button_new();
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, FALSE, 1);
  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_widget_set_usize(area, 16, 16);
  gtk_container_add(GTK_CONTAINER(button), area);    
  
  button = gtk_button_new();
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, FALSE, 1);
  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_widget_set_usize(area, 16, 16);
  gtk_container_add(GTK_CONTAINER(button), area);    
  
  button = gtk_button_new();
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, FALSE, 1);
  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  gtk_widget_set_usize(area, 16, 16);
  gtk_container_add(GTK_CONTAINER(button), area);    

  colsel = gtk_color_selection_new();
  gtk_color_selection_set_update_policy(GTK_COLOR_SELECTION(colsel),
					GTK_UPDATE_DISCONTINUOUS);
  gtk_widget_show(colsel);
  
  gtk_box_pack_start(GTK_BOX(vbox), colsel, FALSE, FALSE, 1);
/*  
  color_selector = colsel;
  h8 = gtk_signal_connect(GTK_OBJECT(colsel), "color_changed", 
			  GTK_SIGNAL_FUNC(e_cb_change_col), colsel);
*/
  return vbox;
}

static GtkWidget *
e_create_gradient_selector(void)
{
  GtkWidget          *hbox, *w, *scrolled_win, *list, *frame, *area, *table,
                     *label, *button, *om, *arrow, *vbox, *entry, *check,
                     *align, *line, *bbox, *carea, *m, *mi, *note, *table2,
                     *vbox2, *win;
  gint                i;
  
  table2 = gtk_table_new(1, 1, FALSE);
  gtk_widget_show(table2);
 
  w = e_create_gradient_editor();
  gtk_widget_show(w);
  gtk_table_attach_defaults(GTK_TABLE(table2), w, 0, 1, 1, 2);

  m = gtk_menu_new();
  gtk_widget_show(m);
  if (backgrounds)
    {
      GList              *ptr;

      ptr = backgrounds;
      while (ptr)
	{
	  Background         *bg;

	  bg = ptr->data;
	  ptr = ptr->next;
	  if ((bg) && (bg->gpmap))
	    {
	      mi = gtk_menu_item_new();
	      gtk_menu_append(GTK_MENU(m), mi);
	      gtk_widget_show(mi);
	      frame = gtk_frame_new(NULL);
	      gtk_widget_show(frame);
	      gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
	      gtk_container_add(GTK_CONTAINER(mi), frame);
	      gtk_container_add(GTK_CONTAINER(frame), bg->gpmap);
	      gtk_widget_show(bg->gpmap);
	      gtk_object_set_data(GTK_OBJECT(mi), "capplet", 
				  (gpointer) current_capplet);
	      bg->gbox = frame;
	      gtk_signal_connect(GTK_OBJECT(mi), "activate", 
				 GTK_SIGNAL_FUNC(e_cb_grad_sel), 
				 (gpointer)bg);
	    }
	}
    }
  om = gtk_option_menu_new();
  gtk_widget_show(om);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), m);
  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 0);

  align = gtk_alignment_new(0.5, 0.5, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_container_add(GTK_CONTAINER(align), om);
  gtk_table_attach_defaults(GTK_TABLE(table2), align, 0, 1, 0, 1);
  
  return table2;
}

static void
func_img_filesel_ok(GtkWidget *widget, gpointer * data)
{
  Background *bg;
  
  if (!data)
    return;
  bg = deskbg[curdesk];
  if (!bg)
    return;
  
  if (bg->bg.file)
    g_free(bg->bg.file);
  if (isfile((gchar *)data))
    {
      gtk_entry_set_text(GTK_ENTRY(bg_bg_file_entry), (gchar *)data);
      bg->bg.file = g_strdup((gchar *)data);
    }
  else
    bg->bg.file = NULL;
  e_cb_bg_apply(NULL, NULL);
}

static void
e_cb_img_browse(GtkWidget * widget, gpointer data)
{
  static GtkWidget *filesel = NULL;
  gchar *file;

  if (!filesel)
    {
      filesel = ee_filesel_new();
      ee_filesel_hide_savedialog(filesel);
      ee_filesel_unset_printout(filesel);
      gtk_signal_connect(GTK_OBJECT(filesel), "ok_clicked",
			 GTK_SIGNAL_FUNC(func_img_filesel_ok), NULL);
    }
  file = gtk_entry_get_text(GTK_ENTRY(bg_bg_file_entry));
  ee_filesel_set_filename(filesel, file);
  gtk_widget_show(filesel);
}

static void
func_overlay_filesel_ok(GtkWidget *widget, gpointer * data)
{
  Background *bg;
  
  if (!data)
    return;
  bg = deskbg[curdesk];
  if (!bg)
    return;
  
  if (bg->top.file)
    g_free(bg->top.file);
  if (isfile((gchar *)data))
    {
      gtk_entry_set_text(GTK_ENTRY(bg_fg_file_entry), (gchar *)data);
      bg->top.file = g_strdup((gchar *)data);
    }
  else
    bg->top.file = NULL;
  e_cb_bg_apply(NULL, NULL);
}

static void
e_cb_overlay_browse(GtkWidget * widget, gpointer data)
{
  static GtkWidget *filesel = NULL;
  gchar *file;

  if (!filesel)
    {
      filesel = ee_filesel_new();
      ee_filesel_hide_savedialog(filesel);
      ee_filesel_unset_printout(filesel);
      gtk_signal_connect(GTK_OBJECT(filesel), "ok_clicked",
			 GTK_SIGNAL_FUNC(func_overlay_filesel_ok), NULL);
    }
  file = gtk_entry_get_text(GTK_ENTRY(bg_fg_file_entry));
  ee_filesel_set_filename(filesel, file);
  gtk_widget_show(filesel);
}

static void
e_cb_img_none(GtkWidget * widget, gpointer data)
{
  gchar *file;
  Background *bg;
  
  bg = deskbg[curdesk];
  if (!bg)
    return;

  if (!bg->bg.file)
    return;
  g_free(bg->bg.file);
  bg->bg.file = NULL;
  gtk_entry_set_text(GTK_ENTRY(bg_bg_file_entry), "");

  e_cb_bg_apply(NULL, NULL);
}

static void
e_cb_overlay_none(GtkWidget * widget, gpointer data)
{
  gchar *file;
  Background *bg;
  
  bg = deskbg[curdesk];
  if (!bg)
    return;

  if (!bg->top.file)
    return;
  g_free(bg->top.file);
  bg->top.file = NULL;
  gtk_entry_set_text(GTK_ENTRY(bg_fg_file_entry), "");

  e_cb_bg_apply(NULL, NULL);
}

static GtkWidget *
e_create_image_selector(void)
{
  GtkWidget          *hbox, *w, *scrolled_win, *list, *frame, *area, *table,
                     *label, *button, *om, *arrow, *vbox, *entry, *check,
                     *align, *line, *bbox, *carea, *m, *mi, *note, *table2,
                     *vbox2, *win;
  gint                i;
  
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  
  frame = gtk_frame_new(_("Image file"));
  gtk_widget_show(frame);
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 1);
  
  hbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox);
  gtk_container_add(GTK_CONTAINER(frame), hbox);
  
  entry = gtk_entry_new_with_max_length(4096);
  bg_bg_file_entry = entry;
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 1);
  gtk_widget_set_sensitive(entry, FALSE);
  
  button = gtk_button_new_with_label(_("Browse"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(e_cb_img_browse), NULL);

  button = gtk_button_new_with_label(_("None"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(e_cb_img_none), NULL);

  frame = gtk_frame_new(_("Image display options"));
  gtk_widget_show(frame);
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 1);
  
  table = gtk_table_new(2, 2, TRUE);
  gtk_widget_show(table);
  gtk_container_add(GTK_CONTAINER(frame), table);
  
  check = gtk_check_button_new_with_label(_("Repeat tiles across screen"));
  bg_bg_tile = check;
  gtk_widget_show(check);
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  h1 = gtk_signal_connect(GTK_OBJECT(check), "toggled", GTK_SIGNAL_FUNC(e_cb_img_onoff), 
			  (gpointer) 0);
  gtk_table_attach_defaults(GTK_TABLE(table), check, 0, 1, 0, 1);
  
  check = gtk_check_button_new_with_label(_("Retain image aspect ratio"));
  bg_bg_aspect = check;
  gtk_widget_show(check);
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  h2 = gtk_signal_connect(GTK_OBJECT(check), "toggled", GTK_SIGNAL_FUNC(e_cb_img_onoff), 
			  (gpointer) 3);
  gtk_table_attach_defaults(GTK_TABLE(table), check, 1, 2, 0, 1);
  
  check = gtk_check_button_new_with_label(_("Maximise height to fit screen"));
  bg_bg_maxv = check;
  gtk_widget_show(check);
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  h3 = gtk_signal_connect(GTK_OBJECT(check), "toggled", GTK_SIGNAL_FUNC(e_cb_img_onoff), 
		     (gpointer) 1);
  gtk_table_attach_defaults(GTK_TABLE(table), check, 0, 1, 1, 2);
  
  check = gtk_check_button_new_with_label(_("Maximise width to fit screen"));
  bg_bg_maxh = check;
  gtk_widget_show(check);
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  h4 = gtk_signal_connect(GTK_OBJECT(check), "toggled", GTK_SIGNAL_FUNC(e_cb_img_onoff), 
			  (gpointer) 2);
  gtk_table_attach_defaults(GTK_TABLE(table), check, 1, 2, 1, 2);
   
  return vbox;
}

static GtkWidget *
e_create_overlay_selector(void)
{
  GtkWidget          *hbox, *w, *scrolled_win, *list, *frame, *area, *table,
                     *label, *button, *om, *arrow, *vbox, *entry, *check,
                     *align, *line, *bbox, *carea, *m, *mi, *note, *table2,
                     *vbox2, *win;
  gint                i;

  
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  
  frame = gtk_frame_new(_("Image file"));
  gtk_widget_show(frame);
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 1);
  
  hbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox);
  gtk_container_add(GTK_CONTAINER(frame), hbox);
  
  entry = gtk_entry_new_with_max_length(4096);
  bg_fg_file_entry = entry;
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 1);
  gtk_widget_set_sensitive(entry, FALSE);
  
  button = gtk_button_new_with_label(_("Browse"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(e_cb_overlay_browse), NULL);

  button = gtk_button_new_with_label(_("None"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(e_cb_overlay_none), NULL);
  
  frame = gtk_frame_new(_("Image display options"));
  gtk_widget_show(frame);
  gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 1);
  
  table = gtk_table_new(2, 2, TRUE);
  gtk_widget_show(table);
  gtk_container_add(GTK_CONTAINER(frame), table);

  m = gtk_menu_new();
  gtk_widget_show(m);
  mi = gtk_menu_item_new_with_label(_("Top left"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)0);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Top middle"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)1);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Top right"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)2);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Right middle"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)3);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Bottom right"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)4);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Bottom middle"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)5);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Bottom left"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)6);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Left middle"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)7);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("Centered"));
  gtk_widget_show(mi);
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", (gpointer) current_capplet);
  gtk_signal_connect(GTK_OBJECT(mi), "activate", 
		     GTK_SIGNAL_FUNC(e_cb_img_position), (gpointer)8);
  gtk_menu_append(GTK_MENU(m), mi);

  om = gtk_option_menu_new();
  bg_fg_pos = om;
  gtk_widget_show(om);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), m);
  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 0);
  gtk_table_attach_defaults(GTK_TABLE(table), om, 0, 1, 0, 1);

  check = gtk_check_button_new_with_label(_("Retain image aspect ratio"));
  bg_fg_aspect = check;
  gtk_widget_show(check);
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  h5 = gtk_signal_connect(GTK_OBJECT(check), "toggled", GTK_SIGNAL_FUNC(e_cb_img_onoff), 
		     (gpointer) 6);
  gtk_table_attach_defaults(GTK_TABLE(table), check, 1, 2, 0, 1);
  
  check = gtk_check_button_new_with_label(_("Maximise height to fit screen"));
  bg_fg_maxv = check;
  gtk_widget_show(check);
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  h6 = gtk_signal_connect(GTK_OBJECT(check), "toggled", GTK_SIGNAL_FUNC(e_cb_img_onoff), 
		     (gpointer) 4);
  gtk_table_attach_defaults(GTK_TABLE(table), check, 0, 1, 1, 2);
  
  check = gtk_check_button_new_with_label(_("Maximise width to fit screen"));
  bg_fg_maxh = check;
  gtk_widget_show(check);
  gtk_object_set_data(GTK_OBJECT(check), "capplet", (gpointer) current_capplet);
  h7 = gtk_signal_connect(GTK_OBJECT(check), "toggled", GTK_SIGNAL_FUNC(e_cb_img_onoff), 
		     (gpointer) 5);
  gtk_table_attach_defaults(GTK_TABLE(table), check, 1, 2, 1, 2);

  return vbox;
}

static void
e_cb_show_edit_win(GtkWidget *widget, gpointer data)
{
  gtk_widget_show(edit_win);
}

static void
e_cb_hide_edit_win(GtkWidget *widget, gpointer data)
{
  gtk_widget_hide(edit_win);
}

static void
e_cb_close_edit_win(GtkWidget *widget, gpointer data)
{
  gtk_widget_hide(widget);
}

static void
e_create_edit_win(void)
{
  GtkWidget          *hbox, *w, *scrolled_win, *list, *frame, *area, *table,
                     *label, *button, *om, *arrow, *vbox, *entry, *check,
                     *align, *line, *bbox, *carea, *m, *mi, *note, *table2,
                     *vbox2, *win;
  gint                i;

  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  edit_win = win;
  gtk_signal_connect(GTK_OBJECT(win), "delete_event",
		     GTK_SIGNAL_FUNC(e_cb_close_edit_win), NULL);
  gtk_window_set_title(GTK_WINDOW(win), _("Enlightenment: Edit Background"));
  gtk_window_set_policy(GTK_WINDOW(win), 0, 0, 1);
  gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_MOUSE);
  
  table = gtk_table_new(1, 1, FALSE);
  gtk_widget_show(table);
  gtk_container_add(GTK_CONTAINER(win), table);
  
  note = gtk_notebook_new();
  gtk_widget_show(note);
  gtk_table_attach(GTK_TABLE(table), note, 0, 1, 1, 2,
		   GTK_FILL, GTK_FILL, 2, 2);  

  /* solid background selector pane */
  label = gtk_label_new(_("Solid Colour"));
  gtk_widget_show(label);
  align = gtk_alignment_new(0.5, 0.5, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_notebook_append_page(GTK_NOTEBOOK(note), align, label);
  note_solid1 = align;
  note_solid2 = label;
  
  w = e_create_solid_selector();
  gtk_container_add(GTK_CONTAINER(align), w);  

  /* gradient background selector pane */
  label = gtk_label_new(_("Gradient"));
  gtk_widget_show(label);
  w = e_create_gradient_selector();
/*  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, label);*/
  note_grad1 = w;
  note_grad2 = label;

  /* Backgrouund Image tile selector */
  label = gtk_label_new(_("Background Image"));
  gtk_widget_show(label);
  w = e_create_image_selector();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, label);
  note_bg1 = w;
  note_bg2 = label;

  /* Overlay Image selector */
  label = gtk_label_new(_("Overlayed Logo"));
  gtk_widget_show(label);
  w = e_create_overlay_selector();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, label);
  
  label = gtk_label_new(_("Please select the attribute of this background\n"
			  "you wish to change below"));
  gtk_widget_show(label);
  gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 0, 1);  

  button = gtk_button_new_with_label(_("Done"));
  gtk_container_set_border_width(GTK_CONTAINER(button), 4);
  gtk_widget_show(button);
  gtk_table_attach(GTK_TABLE(table), button, 0, 1, 2, 3,
		   0, 0, 2, 2);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     GTK_SIGNAL_FUNC(e_cb_hide_edit_win), NULL);
}

static GtkWidget   *
__setup_pane_5(void)
{
  GtkWidget          *hbox, *w, *scrolled_win, *list, *frame, *area, *table,
                     *label, *button, *om, *arrow, *vbox, *entry, *check,
                     *align, *line, *bbox, *carea, *m, *mi, *note, *table2,
                     *vbox2, *tip, *viewport;
  gint                i;

  vbox = gtk_vbox_new(FALSE, 1);
  bg_widget = vbox;
  hbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 1);
  vbox2 = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox2);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, FALSE, TRUE, 1);
  
  w = e_create_onoff(_("High quality rendering for background"), 
		     &e_opt_hq_background);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, TRUE, 1);
  w = e_create_rangeonoff(_("Minutes after which to expunge unviewed backgrounds from memory"),
			  &e_opt_desktop_bg_timeout,
			  0.0, 60.0,
			  _("-"), _("+"),
			  NULL, 0.0);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, TRUE, 1);
  
  e_create_edit_win();
  
  om = gtk_option_menu_new();
  gtk_widget_show(om);
  desk_option_menu = om;
/*  
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), desk_select_menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 0);
 */
  gtk_box_pack_start(GTK_BOX(vbox2), om, FALSE, FALSE, 1);
  tip = (GtkWidget *) gtk_tooltips_new();
  gtk_tooltips_set_tip(GTK_TOOLTIPS(tip), om, 
		       _("Select the desktop you wish to change the "
			 "background of here"), NULL);
  gtk_tooltips_enable(GTK_TOOLTIPS(tip));
  
  e_rebuild_desk_menu();  

  align = gtk_alignment_new(0.5, 0.5, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_box_pack_start(GTK_BOX(vbox2), align, FALSE, FALSE, 1);
  
  button = gtk_button_new_with_label(_("No background"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(e_cb_bg_none), NULL);
  tip = (GtkWidget *) gtk_tooltips_new();
  gtk_tooltips_set_tip(GTK_TOOLTIPS(tip), button, 
		       _("Select no background for this desktop "
			 "(Enlightenment will not attempt to set any "
			 "background for this desktop then)"), NULL);
  gtk_tooltips_enable(GTK_TOOLTIPS(tip));
  
  button = gtk_button_new_with_label(_("Add new..."));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(e_cb_bg_new), NULL);
  tip = (GtkWidget *) gtk_tooltips_new();
  gtk_tooltips_set_tip(GTK_TOOLTIPS(tip), button, 
		       _("Add a new background bookmark to your list"), NULL);
  gtk_tooltips_enable(GTK_TOOLTIPS(tip));

  button = gtk_button_new_with_label(_("Delete"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
		     GTK_SIGNAL_FUNC(e_cb_bg_delete), NULL);
  tip = (GtkWidget *) gtk_tooltips_new();
  gtk_tooltips_set_tip(GTK_TOOLTIPS(tip), button, 
		       _("Remove this background bookmark from the list"), NULL);
  gtk_tooltips_enable(GTK_TOOLTIPS(tip));

  button = gtk_button_new_with_label(_("Edit..."));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     GTK_SIGNAL_FUNC(e_cb_show_edit_win), NULL);
  tip = (GtkWidget *) gtk_tooltips_new();
  gtk_tooltips_set_tip(GTK_TOOLTIPS(tip), button, 
		       _("Edit the settings for this background bookmark"), NULL);
  gtk_tooltips_enable(GTK_TOOLTIPS(tip));

  frame = gtk_frame_new(NULL);
  gtk_widget_show(frame);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
  gtk_container_add(GTK_CONTAINER(align), frame);

  area = gtk_drawing_area_new();
  gtk_widget_show(area);
  desk_mini_display_area = area;
  gtk_drawing_area_size(GTK_DRAWING_AREA(area), 128, 96);
  gtk_widget_set_usize(area, 128, 96);
  gtk_container_add(GTK_CONTAINER(frame), area);
  gtk_signal_connect(GTK_OBJECT(area), "expose_event",
		     GTK_SIGNAL_FUNC(e_cb_expose_mini_desk), NULL);

  scrolled_win = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_show(scrolled_win);
  gtk_box_pack_start(GTK_BOX(hbox), scrolled_win, TRUE, TRUE, 1);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_win),
				 GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  list = gtk_table_new(0, 0, FALSE);
  gtk_widget_show(list);
  image_table_list = list;

  viewport = gtk_viewport_new(NULL, NULL);
  gtk_widget_show(viewport);
  gtk_viewport_set_shadow_type(GTK_VIEWPORT(viewport), GTK_SHADOW_IN);
  gtk_widget_set_usize(viewport, (72 * 4) + 4, -1);
  
  gtk_container_add(GTK_CONTAINER(scrolled_win), viewport);
  gtk_container_add(GTK_CONTAINER(viewport), list);
  if (backgrounds)
    {
      gchar               s[4096];
      int                 i, j, la, ra, ta, ba;
      GList              *ptr;

      j = g_list_length(backgrounds);

      g_snprintf(s, sizeof(s), "%s/.e-conf", getenv("HOME"));
      mkdir(s, S_IRWXU);
      ptr = backgrounds;
      table_size = 4;
      if (j > 2320)
	table_size = (j / 580) + 1;
      for (i = 0; ((i < j) && (ptr)); i++)
	{
	  Background         *bg = NULL;
	  GtkWidget          *l_hb, *l_label, *l_pixmap, *l_item;

	  if (progress)
	    {
	      gtk_progress_bar_update(GTK_PROGRESS_BAR(progress),
				      (gfloat) i / (gfloat) j);
	      gtk_widget_draw(progress, NULL);
	    }
	  bg = ptr->data;
	  ptr = ptr->next;
	  if (bg)
	    {
	      l_item = gtk_button_new();
	      gtk_object_set_data(GTK_OBJECT(l_item), "bg", bg);
	      gtk_object_set_data(GTK_OBJECT(l_item), "capplet", (gpointer) current_capplet);
	      gtk_widget_show(l_item);
	      l_hb = gtk_hbox_new(FALSE, 1);
	      bg->box = l_hb;
	      gtk_container_set_border_width(GTK_CONTAINER(l_hb), 1);
	      gtk_widget_show(l_hb);
	      gtk_container_add(GTK_CONTAINER(l_item), l_hb);
	      l_pixmap = bg->pmap;
	      gtk_widget_show(l_pixmap);
	      gtk_box_pack_start(GTK_BOX(l_hb), l_pixmap, FALSE, FALSE, 0);
	      gtk_signal_connect(GTK_OBJECT(l_item), "clicked",
				 GTK_SIGNAL_FUNC(e_cb_bg_sel), NULL);
	      bg->button = l_item;
	      bg->table = list;
	      ta = i / table_size;
	      ba = ta + 1;
	      la = i % table_size;
	      ra = la + 1;
	      gtk_table_attach_defaults(GTK_TABLE(list), l_item, la, ra, ta, ba);
	    }
	}
    }
  gtk_widget_show(vbox);
  if (is_capp)
    gtk_widget_set_sensitive(bg_widget, FALSE);

  return vbox;
}

/* Pane for audio */
static GtkWidget   *
__setup_pane_6(void)
{
  GtkWidget          *vbox, *w;

  vbox = gtk_vbox_new(FALSE, 1);
  w = e_create_onoff(_("Enable sounds in Enlightenment"),
		     &e_opt_sound);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  gtk_widget_show(vbox);
  return vbox;
}

/* Pane for fonts */
static GtkWidget   *
__setup_pane_7(void)
{
  GtkWidget          *vbox, *w;

  vbox = gtk_vbox_new(FALSE, 1);
  w = gtk_label_new("This page is intentionally blank.\n"
		    "\n"
		    "Widgets will be filled in here shortly");
  gtk_widget_show(w);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  gtk_widget_show(vbox);
  return vbox;
}

/* Pane for colours */
static GtkWidget   *
__setup_pane_8(void)
{
  GtkWidget          *vbox, *w;

  vbox = gtk_vbox_new(FALSE, 1);
  w = gtk_label_new("This page is intentionally blank.\n"
		    "\n"
		    "Widgets will be filled in here shortly");
  gtk_widget_show(w);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  gtk_widget_show(vbox);
  return vbox;
}

/* Pane for cursors */
static GtkWidget   *
__setup_pane_9(void)
{
  GtkWidget          *vbox, *w;

  vbox = gtk_vbox_new(FALSE, 1);
  w = gtk_label_new("This page is intentionally blank.\n"
		    "\n"
		    "Widgets will be filled in here shortly");
  gtk_widget_show(w);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  gtk_widget_show(vbox);
  return vbox;
}

/* Pane for window matches */
static GtkWidget   *
__setup_pane_10(void)
{
  GtkWidget          *vbox, *w;

  vbox = gtk_vbox_new(FALSE, 1);
  w = gtk_label_new("This page is intentionally blank.\n"
		    "\n"
		    "Widgets will be filled in here shortly");
  gtk_widget_show(w);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  gtk_widget_show(vbox);
  return vbox;
}

/* Pane for menus */
static GtkWidget   *
__setup_pane_11(void)
{
  GtkWidget          *vbox, *w;

  vbox = gtk_vbox_new(FALSE, 1);
  w = gtk_label_new("This page is intentionally blank.\n"
		    "\n"
		    "Widgets will be filled in here shortly");
  gtk_widget_show(w);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);

  gtk_widget_show(vbox);
  return vbox;
}

static void
e_cb_theme_list_click(GtkWidget * widget, gint num)
{
  curtheme = num;
  if (!in_init)
    e_changed((current_capplet), FALSE);
}

/* Pane for themes */
static GtkWidget   *
__setup_pane_12(void)
{
  GtkWidget          *vbox, *w, *list, *scrolled_win;
  gchar              *line[1], *name;
  GList              *ptr;
  
  vbox = gtk_vbox_new(FALSE, 1);

  w = gtk_label_new(_("Current theme selection"));
  gtk_widget_show(w);
  gtk_box_pack_start(GTK_BOX(vbox), w, FALSE, FALSE, 1);
  
  scrolled_win = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_show(scrolled_win);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_win),
				 GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize(scrolled_win, -1, 200);
  gtk_box_pack_start(GTK_BOX(vbox), scrolled_win, TRUE, TRUE, 0);

  list = gtk_clist_new(1);
  
  ptr = themes;
  while(ptr)
    {
      line[0] = fileof(ptr->data);
      gtk_clist_append(GTK_CLIST(list), line);
      g_free(line[0]);
      ptr = ptr->next;
    }
  
  gtk_clist_set_selection_mode(GTK_CLIST(list), GTK_SELECTION_BROWSE);
  gtk_clist_columns_autosize(GTK_CLIST(list));
  gtk_clist_select_row(GTK_CLIST(list), curtheme, 0);
  gtk_widget_show(list);
  gtk_container_add(GTK_CONTAINER(scrolled_win), list);
  gtk_signal_connect(GTK_OBJECT(list), "select_row",
		     GTK_SIGNAL_FUNC(e_cb_theme_list_click), NULL);
  gtk_widget_show(vbox);
  return vbox;
}


/************************************************************************/
static gint cur_kb_sel = 0;
static GtkWidget *action_list = NULL;

void
del_kb(Keybind *kb)
{
  keys = g_list_remove(keys, kb);
  if (kb->key)
    g_free(kb->key);
  if (kb->params)
    g_free(kb->params);
  g_free(kb);
  gtk_clist_remove(GTK_CLIST(action_list), cur_kb_sel);
  gtk_clist_select_row(GTK_CLIST(action_list), cur_kb_sel, 0);
  gtk_clist_moveto(GTK_CLIST(action_list),
		   cur_kb_sel, 0,
		   0.5, 0.5);
}

void
add_kb(void)
{
  Keybind *kb = NULL;
  gchar   *line[4];
  
  kb = g_malloc(sizeof(Keybind));
  kb->key = g_strdup("EDIT ME");
  kb->modifier = 0;
  kb->id = 1;
  kb->params = NULL;
  kb->action_id = 0;
  keys = g_list_append(keys, kb);
  
  line[0] = mod_str[kb->modifier];
  line[1] = kb->key;
  line[2] = _(actions[kb->action_id].text);
  if ((kb->params) && (actions[kb->action_id].param_tpe != 0))
    line[3] = kb->params;
  else
    line[3] = "";
  gtk_clist_append(GTK_CLIST(action_list), line);
  gtk_clist_select_row(GTK_CLIST(action_list), 
		       GTK_CLIST(action_list)->rows - 1, 0);
  gtk_clist_moveto(GTK_CLIST(action_list),
		   GTK_CLIST(action_list)->rows - 1, 0,
		   0.5, 0.5);
}

void
update_current_kb(void)
{
  GList *gl;
  Keybind *kb;
  gchar   *line[4];
  
  gl = g_list_nth(keys, cur_kb_sel);
  if (!gl)
    return;
  kb = gl->data;
  line[0] = mod_str[kb->modifier];
  line[1] = kb->key;
  line[2] = _(actions[kb->action_id].text);
  if ((kb->params) && (actions[kb->action_id].param_tpe != 0))
    line[3] = kb->params;
  else
    line[3] = "";
  gtk_clist_set_text(GTK_CLIST(action_list), cur_kb_sel, 0, line[0]);
  gtk_clist_set_text(GTK_CLIST(action_list), cur_kb_sel, 1, line[1]);
  gtk_clist_set_text(GTK_CLIST(action_list), cur_kb_sel, 2, line[2]);
  gtk_clist_set_text(GTK_CLIST(action_list), cur_kb_sel, 3, line[3]);
}

static void
e_cb_modifier(GtkWidget * widget, gpointer data)
{
  gint                value;
  GList *gl;
  Keybind *kb;    
  value = (gint) data;
  
  gl = g_list_nth(keys, cur_kb_sel);
  if (!gl)
    return;
  kb = gl->data;
  kb->modifier = value;
  update_current_kb();
    {
      GtkWidget          *c;
      c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      e_changed(c, FALSE);
    }
}

static void
e_cb_del_kb(GtkWidget *widget, gpointer data)
{
  GList *gl;
  Keybind *kb = NULL;
  
  gl = g_list_nth(keys, cur_kb_sel);
  if (gl)
    {
      kb = gl->data;
      del_kb(kb);
    }
    {
      GtkWidget          *c;
      c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      e_changed(c, FALSE);
    }
  widget = NULL;
  data = NULL;
}

static void
e_cb_new_kb(GtkWidget *widget, gpointer data)
{
  add_kb();
    {
      GtkWidget          *c;
      c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      e_changed(c, FALSE);
    }
  widget = NULL;
  data = NULL;
}

static void
e_cb_action_list_click(GtkWidget * widget, gint num)
{
  GtkWidget          *note;
  GList *gl;
  Keybind *kb = NULL;
  
  note = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "note");
  gtk_notebook_set_page(GTK_NOTEBOOK(note), actions[num].param_tpe);
    {
      GtkWidget          *c;
      c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      e_changed(c, FALSE);
    }
  gl = g_list_nth(keys, cur_kb_sel);
  if (!gl)
    return;
  kb = gl->data;
  kb->action_id = num;
  kb->id = actions[num].id;
  if (kb->params)
    g_free(kb->params);
  kb->params = NULL;
  if (actions[kb->action_id].params)
    kb->params = g_strdup(actions[kb->action_id].params);
  if (actions[kb->action_id].param_tpe == 1)
    kb->params = g_strdup(gtk_entry_get_text(GTK_ENTRY(act_ent1)));
  else if (actions[kb->action_id].param_tpe == 2)
    kb->params = g_strdup(gtk_entry_get_text(GTK_ENTRY(act_ent2)));
  else if (actions[kb->action_id].param_tpe == 3)
    {
      gchar buf[640];
      
      g_snprintf(buf, sizeof(buf), "%s %s",
		 gtk_entry_get_text(GTK_ENTRY(act_ent3)),
		 gtk_entry_get_text(GTK_ENTRY(act_ent4)));
      kb->params = g_strdup(buf);
    }
  update_current_kb();
}

static void
e_cb_entry_change(GtkWidget *widget, gpointer data)
{
  GList *gl;
  Keybind *kb = NULL;
  
  gl = g_list_nth(keys, cur_kb_sel);
  if (!gl)
    return;
  kb = gl->data;
  if (kb->params)
    g_free(kb->params);
  kb->params = NULL;
  if (actions[cur_kb_sel].params)
    kb->params = g_strdup(actions[kb->action_id].params);
  if (actions[kb->action_id].param_tpe == 1)
    kb->params = g_strdup(gtk_entry_get_text(GTK_ENTRY(act_ent1)));
  else if (actions[kb->action_id].param_tpe == 2)
    kb->params = g_strdup(gtk_entry_get_text(GTK_ENTRY(act_ent2)));
  else if (actions[kb->action_id].param_tpe == 3)
    {
      gchar buf[640];
      
      g_snprintf(buf, sizeof(buf), "%s %s",
		 gtk_entry_get_text(GTK_ENTRY(act_ent3)),
		 gtk_entry_get_text(GTK_ENTRY(act_ent4)));
      kb->params = g_strdup(buf);
    }
  update_current_kb();
    {
      GtkWidget          *c;
      c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      e_changed(c, FALSE);
    }
}

static void
e_cb_key_change(GtkWidget *widget, gpointer data)
{
  GList *gl;
  Keybind *kb = NULL;
  GtkWidget *win, *label, *frame, *align;
  
  gl = g_list_nth(keys, cur_kb_sel);
  if (!gl)
    return;
  kb = gl->data;
  
  win = gtk_window_new(GTK_WINDOW_POPUP);
  gtk_window_set_policy(GTK_WINDOW(win), 0, 0, 1);
  gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_MOUSE);
  frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
  align = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
  gtk_container_set_border_width(GTK_CONTAINER(align), 32);
  label = gtk_label_new(_("Please press the key on the keyboard\n"
			  "you wish to modify this keyboard-shortcut\n"
			  "to use from now on."));
  gtk_container_add(GTK_CONTAINER(win), frame);
  gtk_container_add(GTK_CONTAINER(frame), align);
  gtk_container_add(GTK_CONTAINER(align), label);
  gtk_widget_show_all(win);
  while (gtk_events_pending())
    gtk_main_iteration();
  gdk_flush();
  while (gtk_events_pending())
    gtk_main_iteration();
  
    {
      char *key;
      XEvent ev;
      
      gdk_window_set_events(win->window, GDK_KEY_PRESS_MASK);
      XSetInputFocus(GDK_DISPLAY(), GDK_WINDOW_XWINDOW(win->window),
		     RevertToPointerRoot, CurrentTime);
      gdk_keyboard_grab(win->window, TRUE, CurrentTime);
      XWindowEvent(GDK_DISPLAY(), GDK_WINDOW_XWINDOW(win->window), 
		   KeyPressMask, &ev);
      gdk_keyboard_ungrab(gdk_time_get());
      key = XKeysymToString(XKeycodeToKeysym(GDK_DISPLAY(), 
					     ev.xkey.keycode, 0));
      if (kb->key)
	g_free(kb->key);
      kb->key= NULL;
      if (key)
	kb->key = g_strdup(key);
    }
  
  gtk_widget_destroy(win);
  gtk_entry_set_text(GTK_ENTRY(act_key), kb->key);
  update_current_kb();
    {
      GtkWidget          *c;
      c = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "capplet");
      e_changed(c, FALSE);
    }
}

void
add_actions(GtkWidget *list, GtkWidget *note)
{
  GtkWidget          *entry, *align, *vbox;
  gchar              *line[1];
  gint i;
  
  gtk_object_set_data(GTK_OBJECT(list), "note", note);
  for (i = 0; actions[i].text; i++)
    {
      line[0] = _(actions[i].text);
      gtk_clist_append(GTK_CLIST(list), line);
    }
  
  align = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_notebook_append_page(GTK_NOTEBOOK(note), align, NULL);

  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);	  
  gtk_notebook_append_page(GTK_NOTEBOOK(note), vbox, NULL);
  act_ent1 = entry = gtk_entry_new_with_max_length(1024);
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
		     GTK_SIGNAL_FUNC(e_cb_entry_change), NULL);
  gtk_widget_set_usize(entry, 32, -1);
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 1);

  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);	  
  gtk_notebook_append_page(GTK_NOTEBOOK(note), vbox, NULL);
  act_ent2 = entry = gtk_entry_new_with_max_length(1024);
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
		     GTK_SIGNAL_FUNC(e_cb_entry_change), NULL);
  gtk_widget_set_usize(entry, 16, -1);
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 1);

  vbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  gtk_notebook_append_page(GTK_NOTEBOOK(note), vbox, NULL);
  act_ent3 = entry = gtk_entry_new_with_max_length(1024);
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
		     GTK_SIGNAL_FUNC(e_cb_entry_change), NULL);
  gtk_widget_set_usize(entry, 16, -1);
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(vbox), entry, TRUE, TRUE, 1);
  act_ent4 = entry = gtk_entry_new_with_max_length(1024);
  gtk_signal_connect(GTK_OBJECT(entry), "changed",
		     GTK_SIGNAL_FUNC(e_cb_entry_change), NULL);
  gtk_widget_set_usize(entry, 16, -1);
  gtk_widget_show(entry);
  gtk_box_pack_start(GTK_BOX(vbox), entry, TRUE, TRUE, 1);

  gtk_signal_connect(GTK_OBJECT(list), "select_row",
		     GTK_SIGNAL_FUNC(e_cb_action_list_click), NULL);
}

static void
e_cb_action_list_selk(GtkWidget * widget, gint num)
{
  GList *gl;
  Keybind *kb;

  if ((!keys) || (!act_list))
    return;
  cur_kb_sel = num;
  gl = g_list_nth(keys, num);
  if (gl)
    {
      kb = gl->data;
      if (kb->params)
	{
	  if (actions[kb->action_id].param_tpe == 1)
	    gtk_entry_set_text(GTK_ENTRY(act_ent1), kb->params);
	  else if (actions[kb->action_id].param_tpe == 2)
	    gtk_entry_set_text(GTK_ENTRY(act_ent2), kb->params);
	  else if (actions[kb->action_id].param_tpe == 3)
	    {
	      gchar s1[256], s2[256];
	      
	      sscanf(kb->params, "%250s %250s", s1, s2);
	      gtk_entry_set_text(GTK_ENTRY(act_ent3), s1);
	      gtk_entry_set_text(GTK_ENTRY(act_ent4), s2);
	    }
	}
      else
	{
	  gtk_entry_set_text(GTK_ENTRY(act_ent1), "");
	  gtk_entry_set_text(GTK_ENTRY(act_ent2), "");
	  gtk_entry_set_text(GTK_ENTRY(act_ent3), "");
	  gtk_entry_set_text(GTK_ENTRY(act_ent4), "");
	}
      gtk_entry_set_text(GTK_ENTRY(act_key), kb->key);
      gtk_option_menu_set_history(GTK_OPTION_MENU(act_mod), kb->modifier);
      gtk_clist_select_row(GTK_CLIST(act_list), kb->action_id, 0);
    }
}

/* Pane for keybindings */
static GtkWidget   *
__setup_pane_13(void)
{
  GtkWidget          *vbox, *hbox, *sep, *vbox2, *button, *scrolled_win;
  GtkWidget          *list, *hbox2, *label, *table, *align, *entry, *om, *mi;
  GtkWidget          *m, *arrow, *note;
  GList              *ptr;
  gchar              *titles[4] = 
    {
      N_("Modifier"),
      N_("Key"),
      N_("Action to perform"),
      N_("Optional parameters")
    };
  
  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);

  label = gtk_label_new(_("List of keyboard shortcuts"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 1);
  
  hbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 1);

  vbox2 = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox2);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, FALSE, FALSE, 1);

  button = gtk_button_new_with_label(_("New"));
  gtk_object_set_data(GTK_OBJECT(button), "capplet", current_capplet);
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     GTK_SIGNAL_FUNC(e_cb_new_kb), (gpointer)0);
  
  button = gtk_button_new_with_label(_("Delete"));
  gtk_object_set_data(GTK_OBJECT(button), "capplet", current_capplet);
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 1);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     GTK_SIGNAL_FUNC(e_cb_del_kb), (gpointer)0);
  
  scrolled_win = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_show(scrolled_win);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_win),
				 GTK_POLICY_NEVER,
				 GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start(GTK_BOX(hbox), scrolled_win, TRUE, TRUE, 0);
#ifdef ENABLE_NLS
  {
	int i;
	for (i=0;i<4;i++) titles[i]=_(titles[i]);
  } 
#endif
  action_list = list = gtk_clist_new_with_titles(4, titles);
  gtk_clist_set_column_auto_resize(GTK_CLIST(list), 0, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(list), 1, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(list), 2, TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(list), 3, TRUE);
  
  /*******************************************************/
  ptr = keys;
  while(ptr)
    {
      Keybind *kb = NULL;
      gchar              *line[4];

      kb = ptr->data;
      ptr = ptr->next;
      
      line[0] = mod_str[kb->modifier];
      line[1] = kb->key;
      line[2] = _(actions[kb->action_id].text);
      if ((kb->params) && (actions[kb->action_id].param_tpe != 0))
	line[3] = kb->params;
      else
	line[3] = "";
      gtk_clist_append(GTK_CLIST(list), line);
    }
      
  gtk_signal_connect(GTK_OBJECT(list), "select_row",
		     GTK_SIGNAL_FUNC(e_cb_action_list_selk), NULL);
  
  /*******************************************************/  

  gtk_clist_set_selection_mode(GTK_CLIST(list), GTK_SELECTION_BROWSE);
  gtk_clist_columns_autosize(GTK_CLIST(list));
  gtk_widget_show(list);
  gtk_container_add(GTK_CONTAINER(scrolled_win), list);

  /*-----------------------------------------------------*/
  
  sep = gtk_hseparator_new();
  gtk_widget_show(sep);
  gtk_box_pack_start(GTK_BOX(vbox), sep, FALSE, FALSE, 1);
  
  /*-----------------------------------------------------*/
  
  label = gtk_label_new(_("Edit current selected keyboard shortcut"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 1);
  
  hbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 1);

  align = gtk_alignment_new(1.0, 0.0, 0.0, 0.0);
  gtk_widget_show(align);
  gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 1);
  
  table = gtk_table_new(1, 1, FALSE);
  gtk_widget_show(table);
  gtk_container_add(GTK_CONTAINER(align), table);
  gtk_table_set_row_spacings(GTK_TABLE(table), 1);
  gtk_table_set_col_spacings(GTK_TABLE(table), 1);
  
  label = gtk_label_new(_("Key"));
  gtk_widget_show(label);
  gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 0, 1);

  act_key = entry = gtk_entry_new_with_max_length(4096);
  gtk_widget_show(entry);
  gtk_widget_set_usize(entry, 24, -1);
  gtk_widget_set_sensitive(entry, FALSE);
  gtk_table_attach_defaults(GTK_TABLE(table), entry, 1, 2, 0, 1);
  
  button = gtk_button_new_with_label(_("Change..."));
  gtk_object_set_data(GTK_OBJECT(button), "capplet", current_capplet);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     GTK_SIGNAL_FUNC(e_cb_key_change), (gpointer)0);
  gtk_widget_show(button);
  gtk_table_attach_defaults(GTK_TABLE(table), button, 2, 3, 0, 1);
  
  label = gtk_label_new(_("Modifier"));
  gtk_widget_show(label);
  gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 1, 2);

  m = gtk_menu_new();
  gtk_widget_show(m);

  mi = gtk_menu_item_new_with_label(_("NONE"));
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)0);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("CTRL"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)1);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("ALT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)2);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)3);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("CTRL & ALT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)4);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("CTRL & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)5);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("ALT & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)6);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("CTRL & ALT & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)7);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("WIN"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)8);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD3"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)9);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD4"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)10);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD5"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)11);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("WIN & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)12);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("WIN & CTRL"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)13);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("WIN & ALT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)14);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD4 & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)15);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD4 & CTRL"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)16);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD4 & CTRL & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)17);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD5 & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)18);
  gtk_menu_append(GTK_MENU(m), mi);
  mi = gtk_menu_item_new_with_label(_("MOD5 & CTRL"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)19);
  gtk_menu_append(GTK_MENU(m), mi);
/*  
  mi = gtk_menu_item_new_with_label(_("MOD5 & CTRL & SHIFT"));
  gtk_object_set_data(GTK_OBJECT(mi), "capplet", current_capplet);
  gtk_widget_show(mi);
  gtk_signal_connect(GTK_OBJECT(mi), "activate",
		     GTK_SIGNAL_FUNC(e_cb_modifier), (gpointer)20);
  gtk_menu_append(GTK_MENU(m), mi);
*/  
  act_mod =om = gtk_option_menu_new();
  gtk_widget_show(om);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), m);
  gtk_option_menu_set_history(GTK_OPTION_MENU(om), 0);  
  gtk_table_attach_defaults(GTK_TABLE(table), om, 1, 3, 1, 2);

  sep = gtk_vseparator_new();
  gtk_widget_show(sep);
  gtk_box_pack_start(GTK_BOX(hbox), sep, FALSE, FALSE, 1);

  vbox2 = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox2);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, TRUE, TRUE, 1);

  label = gtk_label_new(_("Action to perform"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox2), label, FALSE, FALSE, 1);
  
  scrolled_win = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_show(scrolled_win);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_win),
				 GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize(scrolled_win, 180, 128);
  gtk_box_pack_start(GTK_BOX(vbox2), scrolled_win, TRUE, TRUE, 0);
  act_list = gtk_clist_new(1);
  gtk_clist_set_column_auto_resize(GTK_CLIST(act_list), 0, TRUE);
  gtk_object_set_data(GTK_OBJECT(act_list), "capplet", current_capplet);

  gtk_clist_set_selection_mode(GTK_CLIST(act_list), GTK_SELECTION_BROWSE);
  gtk_clist_columns_autosize(GTK_CLIST(act_list));
  gtk_clist_select_row(GTK_CLIST(act_list), 0, 0);
  gtk_widget_show(act_list);
  gtk_container_add(GTK_CONTAINER(scrolled_win), act_list);

  sep = gtk_hseparator_new();
  gtk_widget_show(sep);
  gtk_table_attach_defaults(GTK_TABLE(table), sep, 1, 3, 2, 3);
  
  label = gtk_label_new(_("Options for Action:"));
  gtk_widget_show(label);
  gtk_table_attach_defaults(GTK_TABLE(table), label, 1, 3, 3, 4);

  note = gtk_notebook_new();
  gtk_widget_show(note);
  gtk_notebook_set_show_tabs(GTK_NOTEBOOK(note), FALSE);
  gtk_table_attach_defaults(GTK_TABLE(table), note, 1, 3, 4, 5);
  
  add_actions(act_list, note);
  
  gtk_clist_select_row(GTK_CLIST(list), 0, 0);
  
  return vbox;
}

static void
e_cb_list_click(GtkWidget * widget, gint num)
{
  GtkWidget          *note;

  note = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "note");
  gtk_notebook_set_page(GTK_NOTEBOOK(note), num);
}

static void
e_setup(GtkWidget * c)
{
  GtkWidget          *frame, *frame2, *hbox, *vbox, *note, *w, *label, *list;
  GtkWidget          *scrolled_win, *pixmap, *tip;
  GdkPixmap          *pmap, *mask;
  gchar              *line[1] =
  {""};

  current_capplet = c;

  hbox = gtk_hbox_new(FALSE, 1);
  gtk_widget_show(hbox);
  if (is_capp)
    gtk_container_add(GTK_CONTAINER(c), hbox);
  else
    {
      vbox = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(c), "vbox");
      gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 2);
    }

  note = gtk_notebook_new();
  gtk_widget_show(note);
  gtk_notebook_set_show_border(GTK_NOTEBOOK(note), FALSE);
  gtk_notebook_set_show_tabs(GTK_NOTEBOOK(note), FALSE);

  vbox = gtk_vbox_new(FALSE, 1);
  gtk_widget_show(vbox);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);

  scrolled_win = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_show(scrolled_win);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_win),
				 GTK_POLICY_AUTOMATIC,
				 GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize(scrolled_win, 140, 160);
  gtk_box_pack_start(GTK_BOX(vbox), scrolled_win, FALSE, FALSE, 0);

  gdk_imlib_data_to_pixmap(e_logo_xpm, &pmap, &mask);
  pixmap = gtk_pixmap_new(pmap, mask);
  gtk_widget_show(pixmap);
  gtk_box_pack_start(GTK_BOX(vbox), pixmap, TRUE, TRUE, 0);
  list = gtk_clist_new(1);
  section_clist = list;
  gtk_clist_set_row_height(GTK_CLIST(list), 16);
  gtk_clist_set_selection_mode(GTK_CLIST(list), GTK_SELECTION_BROWSE);

  gtk_object_set_data(GTK_OBJECT(list), "note", note);

  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(focus_and_display_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 0, 0,
			_("Basic Options"), 1, pmap, mask);
  w = __setup_pane_1();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(desktops_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 1, 0,
			_("Desktops"), 1, pmap, mask);
  w = __setup_pane_4();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(behavior_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 2, 0,
			_("Behavior"), 1, pmap, mask);
  w = __setup_pane_3();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(audio_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 3, 0,
			_("Audio"), 1, pmap, mask);
  w = __setup_pane_6();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(special_effects_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 4, 0,
			_("Special FX"), 1, pmap, mask);
  w = __setup_pane_2();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(backgrounds_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 5, 0,
			_("Backgrounds"), 1, pmap, mask);
  w = __setup_pane_5();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
/*  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(fonts_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 6, 0,
			_("Fonts"), 1, pmap, mask);
  w = __setup_pane_7();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(colours_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 7, 0,
			_("Colours"), 1, pmap, mask);
  w = __setup_pane_8();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(cursors_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 8, 0,
			_("Mouse Cursors"), 1, pmap, mask);
  w = __setup_pane_9();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(borders_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 9, 0,
			_("Borders"), 1, pmap, mask);
  w = __setup_pane_10();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(menus_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 10, 0,
			_("Menus"), 1, pmap, mask);
  w = __setup_pane_11();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
*/  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(borders_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 6, 0,
			_("Themes"), 1, pmap, mask);
  w = __setup_pane_12();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  
  gtk_clist_append(GTK_CLIST(list), line);
  gdk_imlib_data_to_pixmap(fonts_xpm, &pmap, &mask);
  gtk_clist_set_pixtext(GTK_CLIST(list), 7, 0,
			_("Shortcuts"), 1, pmap, mask);
  w = __setup_pane_13();
  gtk_notebook_append_page(GTK_NOTEBOOK(note), w, NULL);
  

  if (is_capp)
    gtk_clist_set_selectable(GTK_CLIST(list), 5, FALSE);
  
  gtk_clist_columns_autosize(GTK_CLIST(list));
  gtk_clist_select_row(GTK_CLIST(list), 0, 0);
  gtk_widget_show(list);
  gtk_container_add(GTK_CONTAINER(scrolled_win), list);

  gtk_signal_connect(GTK_OBJECT(list), "select_row",
		     GTK_SIGNAL_FUNC(e_cb_list_click), note);

  gtk_box_pack_start(GTK_BOX(hbox), note, TRUE, TRUE, 0);
}

static void
e_setup_multi(GtkWidget * old, GtkWidget * c)
{
  if (is_capp)
    {
      gtk_signal_connect(GTK_OBJECT(c), "new_multi_capplet",
			 GTK_SIGNAL_FUNC(e_setup_multi), NULL);
      switch (CAPPLET_WIDGET(c)->capid)
	{
	 default:
	 case -1:
	 case 0:
	  e_setup(c);
	  e_init_capplet(c, e_try, e_revert, e_ok, e_cancel);
	  break;
	}
    }
  else
    {
      GtkWidget *vbox;
      vbox = gtk_vbox_new(FALSE, 1);
      gtk_widget_show(vbox);
      gtk_container_add(GTK_CONTAINER(c), vbox);
      gtk_object_set_data(GTK_OBJECT(c), "vbox", vbox);
      e_setup(c);
      e_init_capplet(c, e_try, e_revert, e_ok, e_cancel);
    }
}

void
e_cb_quit(GtkWidget *widget, gpointer data)
{
  exit(1);
}

int
main(int argc, char **argv)
{
  GtkWidget          *capplet;
  gint                i;

  for (i = 0; i < 32; i++)
    {
      deskbg[i] = NULL;
      deskbgchanged[i] = 0;
    }

  is_capp = 0;
  for (i = 1; i < argc; i++)
    {
      if (!strstr(argv[i], "--cap-id"))
	is_capp = 1;
    }

  if (is_capp)
    {
      bindtextdomain("E-conf", GNOMELOCALEDIR);
      textdomain("E-conf");
      gnome_capplet_init("E-conf", "0.15", argc,
			 argv, NULL, 0, NULL);
      capplet = capplet_widget_new();
    }
  else
    {
      bindtextdomain("E-conf", GNOMELOCALEDIR);
      textdomain("E-conf");
      gnome_init("E-conf", "0.15", argc, argv);
      capplet = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      gtk_window_set_title(GTK_WINDOW(capplet), 
			   _("Enlightenment Configuration Editor"));
      gtk_window_set_policy(GTK_WINDOW(capplet), 0, 1, 1);
    }
  gtk_widget_realize(capplet);

  if (!CommsInit(recieve_ipc_msg))
    {
      GtkWidget *win, *label, *align, *frame, *button, *vbox;
      
      win = gtk_window_new(GTK_WINDOW_POPUP);
      gtk_window_set_policy(GTK_WINDOW(win), 0, 0, 1);
      gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_CENTER);
      frame = gtk_frame_new(NULL);
      gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_OUT);
      align = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
      gtk_container_set_border_width(GTK_CONTAINER(align), 32);
      vbox = gtk_vbox_new(FALSE, 5);
      button = gtk_button_new_with_label(_("Quit"));
      gtk_signal_connect(GTK_OBJECT(button), "clicked",
			 GTK_SIGNAL_FUNC(e_cb_quit), NULL);
      label = gtk_label_new(_("You are not running Enlightenment\n"
			      "\n"
			      "This window manager has to be running in order\n"
			      "to configure it.\n"
			      "\n"
			      ));
      gtk_container_add(GTK_CONTAINER(win), frame);
      gtk_container_add(GTK_CONTAINER(frame), align);
      gtk_container_add(GTK_CONTAINER(align), vbox);
      gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
      gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
      gtk_widget_show_all(win);
      gtk_main();
      exit(1);
    }
  CommsSend("set clientname Enlightenment Configuration Utility");
  CommsSend("set version 0.2.0");
  CommsSend("set author The Rasterman (Carsten Haitzler)");
  CommsSend("set email raster@rasterman.com");
  CommsSend("set web http://www.rasterman.com/");
  CommsSend("set address C/O Red Hat Software, USA");
  CommsSend("set info "
	    "This is the Enlightenemnt Configuration Utility\n"
	    "that uses Enlightenment's IPC mechanism to configure\n"
	    "it remotely.");

  progress_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(progress_win), 
		       _("Scanning Enlightenment Configuration..."));
  gtk_window_set_policy(GTK_WINDOW(progress_win), 0, 0, 1);
  progress = gtk_progress_bar_new();
  gtk_widget_show(progress);
  gtk_widget_set_usize(progress, 320, 16);
  gtk_container_add(GTK_CONTAINER(progress_win), progress);
  gtk_window_set_position(GTK_WINDOW(progress_win), GTK_WIN_POS_CENTER);
  gtk_widget_show(progress_win);

  e_read();
  if (backgrounds)
    {
      gchar               s[4096];
      int                 i, j, la, ra, ta, ba;
      GList              *ptr;

      j = g_list_length(backgrounds);

      g_snprintf(s, sizeof(s), "%s/.e-conf", getenv("HOME"));
      mkdir(s, S_IRWXU);
      ptr = backgrounds;
      for (i = 0; ((i < j) && (ptr)); i++)
	{
	  Background         *bg = NULL;
	  GdkPixmap          *pmap;
	  GtkWidget          *l_hb, *l_label, *l_pixmap, *l_item;
	  GdkImlibImage      *im;

	  if (progress)
	    {
	      gtk_progress_bar_update(GTK_PROGRESS_BAR(progress),
				      (gfloat) i / (gfloat) j);
	      gtk_widget_draw(progress, NULL);
	    }
	  pmap = gdk_pixmap_new(capplet->window, 64, 48, -1);
	  bg = ptr->data;
	  ptr = ptr->next;
	  if (bg)
	    {
	      g_snprintf(s, sizeof(s), "%s/.e-conf/%s", getenv("HOME"), bg->name);
	      im = gdk_imlib_load_image(s);
	      if (!im)
		{
		  e_render_bg_onto(pmap, bg);
		  im = gdk_imlib_create_image_from_drawable(pmap, NULL, 0, 0,
							    64, 48);
		  if (im)
		    {
		      gdk_imlib_save_image_to_ppm(im, s);
		      gdk_imlib_kill_image(im);
		    }
		}
	      else
		{
		  gdk_imlib_paste_image(im, pmap, 0, 0, 64, 48);
		  gdk_imlib_destroy_image(im);
		}
	    }
	  l_pixmap = gtk_pixmap_new(pmap, NULL);
	  bg->pmap = l_pixmap;
	  bg->gpmap = NULL;
	  if (!strncmp(bg->name, "GRADIENT.", 9))
	    {
	      l_pixmap = gtk_pixmap_new(pmap, NULL);
	      bg->gpmap = l_pixmap;
	    }
	  gdk_pixmap_unref(pmap);
	}
    }
  e_setup_multi(NULL, capplet);
  gtk_widget_destroy(progress_win);
  progress_win = NULL;
  progress = NULL;
  in_init = 0;
  if (is_capp)
    {
      gtk_widget_show(capplet);
      capplet_gtk_main();
    }
  else
    {
      gtk_widget_show(capplet);
      gtk_main();
    }
  return 0;
}
