# Note that this is NOT a relocatable package
%define ver      0.13
%define rel      1
%define prefix   /usr

Summary: Enlightenment Configuration applet.
Name: enlightenment-conf
Version: %ver
Release: %rel
Copyright: GPL
Group: X11/Libraries
Source: ftp://www.rasterman.com/pub/enlightenment/enlightenment-conf-%{ver}.tar.gz
BuildRoot: /var/tmp/e-conf-root
Packager: The Rasterman <raster@redhat.com>
URL: http://www.rasterman.com
Docdir: %{prefix}/doc
Requires: gnome-core >= 1.0.0
Requires: ORBit >= 0.4.0
Requires: control-center >= 1.0.0

%description
A Configuration tool for easily setting up Enlightenment

%changelog

* Wed Feb 3 1999 The Rasterman <raster@redhat.com>

- Created for new E-conf standalone module

%prep
%setup

%build
%ifarch alpha
  MYARCH_FLAGS="--host=alpha-redhat-linux"
%endif
CFLAGS="$RPM_OPT_FLAGS" ./configure $MYARCH_FLAGS --prefix=%prefix
make

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
#rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/bin/*
%{prefix}/share/control-center/Desktop/Enlightenment.desktop
%{prefix}/share/locale/*/*/*
