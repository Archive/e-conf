# control-center fr.po
# Copyright (C) 1998 Free Software Foundation, Inc.
# Vincent Renardias <vincent@debian.org>, 1998, 1999.
#
msgid ""
msgstr ""
"Project-Id-Version: E-conf 0.15\n"
"POT-Creation-Date: 1999-08-31 18:04+0200\n"
"PO-Revision-Date: 1998-02-28 19:34+0100\n"
"Last-Translator: Vincent Renardias <vincent@debian.org>\n"
"Language-Team: Vincent Renardias <vincent@debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: e-conf.c:216
#, fuzzy
msgid "Run command"
msgstr "Commande"

#: e-conf.c:218
#, fuzzy
msgid "Restart Enlightenment"
msgstr "Activer les effets sonores dans Enlightenment"

#: e-conf.c:219
#, fuzzy
msgid "Exit Enlightenment"
msgstr "Activer les effets sonores dans Enlightenment"

#: e-conf.c:221
msgid "Goto Next Desktop"
msgstr ""

#: e-conf.c:222
msgid "Goto Previous Deskop"
msgstr ""

#: e-conf.c:223
#, fuzzy
msgid "Goto Desktop"
msgstr "S�parer les bureaux"

#: e-conf.c:224
#, fuzzy
msgid "Raise Desktop"
msgstr "S�parer les bureaux"

#: e-conf.c:225
#, fuzzy
msgid "Lower Desktop"
msgstr "S�parer les bureaux"

#: e-conf.c:226
msgid "Reset Desktop In Place"
msgstr ""

#: e-conf.c:228
msgid "Toggle Deskrays"
msgstr ""

#: e-conf.c:230
msgid "Cleanup Windows"
msgstr ""

#: e-conf.c:231
msgid "Scroll Windows to left"
msgstr ""

#: e-conf.c:232
msgid "Scroll Windows to right"
msgstr ""

#: e-conf.c:233
msgid "Scroll Windows up"
msgstr ""

#: e-conf.c:234
msgid "Scroll Windows down"
msgstr ""

#: e-conf.c:235
msgid "Scroll Windows by [X Y] pixels"
msgstr ""

#: e-conf.c:237
#, fuzzy
msgid "Move mouse pointer to left"
msgstr "Au pointeur de la souris"

#: e-conf.c:238
#, fuzzy
msgid "Move mouse pointer to right"
msgstr "Au pointeur de la souris"

#: e-conf.c:239
#, fuzzy
msgid "Move mouse pointer up"
msgstr "Au pointeur de la souris"

#: e-conf.c:240
#, fuzzy
msgid "Move mouse pointer down"
msgstr "Au pointeur de la souris"

#: e-conf.c:241
#, fuzzy
msgid "Move mouse pointer by [X Y]"
msgstr "Au pointeur de la souris"

#: e-conf.c:243
msgid "Goto Desktop area [X Y]"
msgstr ""

#: e-conf.c:244
msgid "Move to Desktop area on the left"
msgstr ""

#: e-conf.c:245
msgid "Move to Desktop area on the right"
msgstr ""

#: e-conf.c:246
msgid "Move to Desktop area above"
msgstr ""

#: e-conf.c:247
msgid "Move to Desktop area below"
msgstr ""

#: e-conf.c:249
msgid "Raise Window"
msgstr ""

#: e-conf.c:250
msgid "Lower Window"
msgstr ""

#: e-conf.c:251
msgid "Close Window"
msgstr ""

#: e-conf.c:252
msgid "Annihilate Window"
msgstr ""

#: e-conf.c:253
msgid "Stick / Unstick Window"
msgstr ""

#: e-conf.c:254
msgid "Iconify Window"
msgstr ""

#: e-conf.c:255
msgid "Shade / Unshade Window"
msgstr ""

#: e-conf.c:256
msgid "Maximise Height of Window"
msgstr ""

#: e-conf.c:257
msgid "Maximise Height of Window to whole screen"
msgstr ""

#: e-conf.c:258
msgid "Maximise Height of Window toavailable space"
msgstr ""

#: e-conf.c:259
msgid "Maximise Width of Window"
msgstr ""

#: e-conf.c:260
msgid "Maximise Width of Window to whole screen"
msgstr ""

#: e-conf.c:261
msgid "Maximise Width of Window toavailable space"
msgstr ""

#: e-conf.c:262
msgid "Maximise Size of Window"
msgstr ""

#: e-conf.c:263
msgid "Maximise Size of Window to whole screen"
msgstr ""

#: e-conf.c:264
msgid "Maximise Size of Window toavailable space"
msgstr ""

#: e-conf.c:265
msgid "Send window to next desktop"
msgstr ""

#: e-conf.c:266
msgid "Send window to previous desktop"
msgstr ""

#: e-conf.c:267
msgid "Switch focus to next window"
msgstr ""

#: e-conf.c:268
msgid "Switch focus to previous window"
msgstr ""

#: e-conf.c:269
msgid "Glue / Unglue Window to Desktop screen"
msgstr ""

#: e-conf.c:270
msgid "Set Window layer to On Top"
msgstr ""

#: e-conf.c:271
msgid "Set Window layer to Above"
msgstr ""

#: e-conf.c:272
msgid "Set Window layer to Normal"
msgstr ""

#: e-conf.c:273
msgid "Set Window layer to Below"
msgstr ""

#: e-conf.c:274
msgid "Set Window layer"
msgstr ""

#: e-conf.c:275
msgid "Move Window to area on left"
msgstr ""

#: e-conf.c:276
msgid "Move Window to area on right"
msgstr ""

#: e-conf.c:277
msgid "Move Window to area above"
msgstr ""

#: e-conf.c:278
msgid "Move Window to area below"
msgstr ""

#: e-conf.c:279
msgid "Move Window by area [X Y]"
msgstr ""

#: e-conf.c:281
msgid "Set Window border style to the Default"
msgstr ""

#: e-conf.c:282
msgid "Set Window border style to the Borderless"
msgstr ""

#: e-conf.c:284
msgid "Forget everything about Window"
msgstr ""

#: e-conf.c:285
msgid "Remember all Window settings"
msgstr ""

#: e-conf.c:286
msgid "Remember Window Border"
msgstr ""

#: e-conf.c:287
msgid "Remember Window Desktop"
msgstr ""

#: e-conf.c:288
msgid "Remember Window Desktop Area"
msgstr ""

#: e-conf.c:289
msgid "Remember Window Size"
msgstr ""

#: e-conf.c:290
msgid "Remember Window Location"
msgstr ""

#: e-conf.c:291
msgid "Remember Window Layer"
msgstr ""

#: e-conf.c:292
msgid "Remember Window Stickyness"
msgstr ""

#: e-conf.c:293
msgid "Remember Window Shadedness"
msgstr ""

#: e-conf.c:295
msgid "Show Root Menu"
msgstr ""

#: e-conf.c:296
msgid "Show Winops Menu"
msgstr ""

#: e-conf.c:297
msgid "Show Named Menu"
msgstr ""

#: e-conf.c:299
msgid "Goto Linear Area"
msgstr ""

#: e-conf.c:300
msgid "Previous Linear Area"
msgstr ""

#: e-conf.c:301
msgid "Next Linear Area"
msgstr ""

#: e-conf.c:308 e-conf.c:4628
msgid "CTRL"
msgstr ""

#: e-conf.c:309 e-conf.c:4634
#, fuzzy
msgid "ALT"
msgstr "CAPPLET"

#: e-conf.c:310 e-conf.c:4640
msgid "SHIFT"
msgstr ""

#: e-conf.c:311
msgid "CTRL+ALT"
msgstr ""

#: e-conf.c:312
msgid "CTRL+SHIFT"
msgstr ""

#: e-conf.c:313
msgid "ALT+SHIFT"
msgstr ""

#: e-conf.c:314
msgid "CTRL+ALT+SHIFT"
msgstr ""

#: e-conf.c:315 e-conf.c:4670
msgid "WIN"
msgstr ""

#: e-conf.c:316 e-conf.c:4676
#, fuzzy
msgid "MOD3"
msgstr "MODE"

#: e-conf.c:317 e-conf.c:4682
#, fuzzy
msgid "MOD4"
msgstr "MODE"

#: e-conf.c:318 e-conf.c:4688
#, fuzzy
msgid "MOD5"
msgstr "MODE"

#: e-conf.c:319
msgid "WIN+SHIFT"
msgstr ""

#: e-conf.c:320
msgid "WIN+CTRL"
msgstr ""

#: e-conf.c:321
#, fuzzy
msgid "WIN+ALT"
msgstr "CAPPLET"

#: e-conf.c:322
msgid "MOD4+SHIFT"
msgstr ""

#: e-conf.c:323
msgid "MOD4+CTRL"
msgstr ""

#: e-conf.c:324
msgid "MOD4+ALT"
msgstr ""

#: e-conf.c:325
msgid "MOD4+CTRL+SHIFT"
msgstr ""

#: e-conf.c:326
msgid "MOD5+SHIFT"
msgstr ""

#: e-conf.c:327
msgid "MOD5+CTRL"
msgstr ""

#: e-conf.c:1375
msgid "Apply"
msgstr ""

#.
#. revertb = gtk_button_new_with_label (_("Revert"));
#. gtk_container_add (GTK_CONTAINER (bbox), revertb);
#.
#: e-conf.c:1382
msgid "OK"
msgstr "OK"

#: e-conf.c:1386 e-conf_filesel.c:611
msgid "Cancel"
msgstr "Annuler"

#: e-conf.c:1668 e-conf.c:2156
msgid "Enable"
msgstr ""

#: e-conf.c:1812
#, c-format
msgid "%1.0f Desktop"
msgstr ""

#: e-conf.c:1814
#, c-format
msgid "%1.0f Desktops"
msgstr ""

#: e-conf.c:1841 e-conf.c:1894
#, c-format
msgid ""
"The virtual desktop size will be\n"
"%1.0f x %1.0f\n"
"screens in size"
msgstr ""

#: e-conf.c:1902
msgid "Edge flip resistance (0.01 sec)"
msgstr ""

#: e-conf.c:1905 e-conf.c:2107 e-conf.c:2113 e-conf.c:2119 e-conf.c:2125
#: e-conf.c:2219 e-conf.c:2225 e-conf.c:3802
msgid "+"
msgstr "+"

#: e-conf.c:1905 e-conf.c:2107 e-conf.c:2113 e-conf.c:2119 e-conf.c:2125
#: e-conf.c:2219 e-conf.c:2225 e-conf.c:3802
msgid "-"
msgstr "-"

#: e-conf.c:1964
msgid ""
"The number of separate desktops\n"
"layered on top of eachother"
msgstr ""

#: e-conf.c:1990 e-conf.c:2005 e-conf.c:2084
msgid "Opaque"
msgstr ""

#: e-conf.c:1991
msgid "Windows are moved in a solid fashion"
msgstr ""

#: e-conf.c:1992 e-conf.c:2007 e-conf.c:2086
#, fuzzy
msgid "Lined"
msgstr "Mosa�que"

#: e-conf.c:1993
msgid "Windows are moved by drawing construction lines around them"
msgstr ""

#: e-conf.c:1994 e-conf.c:2009 e-conf.c:2088
msgid "Box"
msgstr ""

#: e-conf.c:1995
msgid "Windows are moved by drawing a box as their outline"
msgstr ""

#: e-conf.c:1996 e-conf.c:2011 e-conf.c:2090
#, fuzzy
msgid "Shaded"
msgstr "Redimentionn�e"

#: e-conf.c:1997
msgid "Windows are moved by drawing a stippled outline of them"
msgstr ""

#: e-conf.c:1998 e-conf.c:2013 e-conf.c:2092
msgid "Semi-Solid"
msgstr ""

#: e-conf.c:1999
msgid "Windows are moved by drawing a chequered outline of them"
msgstr ""

#: e-conf.c:2000
msgid "Translucent"
msgstr ""

#: e-conf.c:2001
msgid "Windows are moved by drawing a translucent copy of the window"
msgstr ""

#: e-conf.c:2006
msgid "Windows are resized as you drag"
msgstr ""

#: e-conf.c:2008
msgid "Windows get resized by displaying construction lines around them"
msgstr ""

#: e-conf.c:2010
msgid "Windows are resized by drawing a box as their outline"
msgstr ""

#: e-conf.c:2012
msgid "windows are resized by drawing a stippled outline of them"
msgstr ""

#: e-conf.c:2014
msgid "Windows are resized by drawing a chequered outline of them"
msgstr ""

#: e-conf.c:2018
#, fuzzy
msgid "Mouse Pointer"
msgstr "D�placement de la souris"

#: e-conf.c:2019
msgid ""
"Your keypresses are sent to whatever window your mouse is over at the time"
msgstr ""

#: e-conf.c:2020
msgid "Sloppy Pointer"
msgstr ""

#: e-conf.c:2021
msgid "Your keypresses are sent to the window your mouse was over last, if any"
msgstr ""

#: e-conf.c:2022
msgid "Pointer Clicks"
msgstr ""

#: e-conf.c:2023
msgid "You keypresses are sent to the window you last clicked on"
msgstr ""

#: e-conf.c:2028
msgid "Move Methods"
msgstr "M�thodes de d�placement"

#: e-conf.c:2031
msgid "Resize Methods"
msgstr "M�thodes de redimensionnement"

#: e-conf.c:2034
msgid "Keyboard focus follows"
msgstr ""

#: e-conf.c:2039
msgid "Reset all settings to system defaults and exit"
msgstr ""

#: e-conf.c:2085
msgid "Windows are slid in a solid fashion"
msgstr ""

#: e-conf.c:2087
msgid "Windows are slid by drawing construction lines around them"
msgstr ""

#: e-conf.c:2089
msgid "Windows are slid by drawing a box as their outline"
msgstr ""

#: e-conf.c:2091
msgid "Windows are slid by drawing a stippled outline of them"
msgstr ""

#: e-conf.c:2093
msgid "Windows are slid by drawing a chequered outline of them"
msgstr ""

#: e-conf.c:2101
msgid "Window Sliding Methods"
msgstr ""

#: e-conf.c:2104
msgid "Windows slide in when they appear"
msgstr ""

#: e-conf.c:2110
msgid "Windows slide about during window cleanup"
msgstr ""

#: e-conf.c:2116
msgid "Desktops slide in when changing desktops"
msgstr ""

#: e-conf.c:2122
msgid "Window shading speed (pixels / sec)"
msgstr ""

#: e-conf.c:2130
msgid "Enable Enlightenments background selection"
msgstr ""

#: e-conf.c:2134
msgid ""
"This enables Enlightenments background selector. If backgrounds are selected "
"in this section any backgrounds selected in any other selector or set by any "
"other programs on desktop 0 will be overidden by Enlightenment"
msgstr ""

#: e-conf.c:2150
msgid "Drag bar"
msgstr ""

#: e-conf.c:2166
msgid "Location:"
msgstr ""

#: e-conf.c:2167
msgid ""
"Left\n"
"Right\n"
"Top\n"
"Bottom"
msgstr ""

#: e-conf.c:2171
msgid "Animate menus"
msgstr "Animer les menus"

#: e-conf.c:2173
msgid ""
"Reduce refresh\n"
"(use SaveUnders)"
msgstr ""

#: e-conf.c:2191
msgid "Advanced Focus"
msgstr ""

#: e-conf.c:2194
msgid "All new windows that appear get the keyboard focus"
msgstr "Les nouvelles fen�tres obtiennent le focus clavier"

#: e-conf.c:2197
msgid "All new popup windows get the keyboard focus"
msgstr ""

#: e-conf.c:2200
msgid "Only new popup windows whose owner is focused get the keyboard focus"
msgstr ""

#: e-conf.c:2203
msgid "Raise windows when switching focus with the keyboard"
msgstr ""

#: e-conf.c:2206
msgid "Send the pointer to windows when switching focus with the keyboard"
msgstr ""

#: e-conf.c:2213
msgid "Miscellaneous"
msgstr ""

#: e-conf.c:2216
msgid "Tooltips ON/OFF & timeout for tooltip popup (sec)"
msgstr ""

#: e-conf.c:2222
msgid "Automatic raising of windows after X seconds"
msgstr ""

#: e-conf.c:2228
msgid "Transient popup windows appear together with leader"
msgstr ""

#: e-conf.c:2231
msgid "Switch to where popup window appears"
msgstr ""

#: e-conf.c:2234
msgid "Display icons when windows are iconified"
msgstr "Afficher une icone pour les fen�tres iconifi�es"

#: e-conf.c:2237
msgid "Place windows manually"
msgstr ""

#: e-conf.c:2252
msgid "Size of Virtual Screen"
msgstr "Taille de l'�cran virtuel"

#: e-conf.c:2265
msgid "Separate Desktops"
msgstr "S�parer les bureaux"

#: e-conf.c:2597
#, fuzzy, c-format
msgid "Desktop %i"
msgstr "S�parer les bureaux"

#: e-conf.c:3188
msgid "Number of colours in gradient"
msgstr ""

#: e-conf.c:3196
msgid "2"
msgstr ""

#: e-conf.c:3201
msgid "3"
msgstr ""

#: e-conf.c:3206
msgid "5"
msgstr ""

#: e-conf.c:3479 e-conf.c:3561
msgid "Image file"
msgstr "Fichier image"

#: e-conf.c:3493 e-conf.c:3575
msgid "Browse"
msgstr "Parcourir"

#: e-conf.c:3499 e-conf.c:3581
msgid "None"
msgstr "Aucun"

#: e-conf.c:3505 e-conf.c:3587
msgid "Image display options"
msgstr "Options de l'affichage des images"

#: e-conf.c:3513
msgid "Repeat tiles across screen"
msgstr ""

#: e-conf.c:3521 e-conf.c:3659
msgid "Retain image aspect ratio"
msgstr ""

#: e-conf.c:3529 e-conf.c:3667
msgid "Maximise height to fit screen"
msgstr ""

#: e-conf.c:3537 e-conf.c:3675
msgid "Maximise width to fit screen"
msgstr ""

#: e-conf.c:3597
#, fuzzy
msgid "Top left"
msgstr "1er niveau"

#: e-conf.c:3603
msgid "Top middle"
msgstr ""

#: e-conf.c:3609
msgid "Top right"
msgstr ""

#: e-conf.c:3615
#, fuzzy
msgid "Right middle"
msgstr "Droitier"

#: e-conf.c:3621
#, fuzzy
msgid "Bottom right"
msgstr "Bas"

#: e-conf.c:3627
#, fuzzy
msgid "Bottom middle"
msgstr "Bas"

#: e-conf.c:3633
#, fuzzy
msgid "Bottom left"
msgstr "Bas"

#: e-conf.c:3639
#, fuzzy
msgid "Left middle"
msgstr "Gaucher"

#: e-conf.c:3645
msgid "Centered"
msgstr "Centr�"

#: e-conf.c:3717
msgid "Enlightenment: Edit Background"
msgstr ""

#. solid background selector pane
#: e-conf.c:3731
msgid "Solid Colour"
msgstr "Couleur unie"

#. gradient background selector pane
#: e-conf.c:3743
msgid "Gradient"
msgstr "Gradient"

#. Backgrouund Image tile selector
#: e-conf.c:3751
msgid "Background Image"
msgstr "Image de Fond"

#. Overlay Image selector
#: e-conf.c:3759
msgid "Overlayed Logo"
msgstr "Logo superpos�"

#: e-conf.c:3764
msgid ""
"Please select the attribute of this background\n"
"you wish to change below"
msgstr ""

#: e-conf.c:3769
msgid "Done"
msgstr "Effectu�"

#: e-conf.c:3796
msgid "High quality rendering for background"
msgstr ""

#: e-conf.c:3799
msgid "Minutes after which to expunge unviewed backgrounds from memory"
msgstr ""

#: e-conf.c:3818
msgid "Select the desktop you wish to change the background of here"
msgstr ""

#: e-conf.c:3828
msgid "No background"
msgstr "Pas de fond d'�cran"

#: e-conf.c:3835
msgid ""
"Select no background for this desktop (Enlightenment will not attempt to set "
"any background for this desktop then)"
msgstr ""

#: e-conf.c:3840
msgid "Add new..."
msgstr "Ajouter un nouveau..."

#: e-conf.c:3847
msgid "Add a new background bookmark to your list"
msgstr "Ajouter un nouveau signet de fond d'�cran � votre liste"

#: e-conf.c:3850 e-conf.c:4518
msgid "Delete"
msgstr "Effacer"

#: e-conf.c:3857
msgid "Remove this background bookmark from the list"
msgstr "Enlever ce signet de la liste"

#: e-conf.c:3860
msgid "Edit..."
msgstr "Editer..."

#: e-conf.c:3867
msgid "Edit the settings for this background bookmark"
msgstr "Editer les param�tres de ce signet"

#: e-conf.c:3968
msgid "Enable sounds in Enlightenment"
msgstr "Activer les effets sonores dans Enlightenment"

#: e-conf.c:4079
#, fuzzy
msgid "Current theme selection"
msgstr "Centre de l'�cran"

#: e-conf.c:4338
msgid ""
"Please press the key on the keyboard\n"
"you wish to modify this keyboard-shortcut\n"
"to use from now on."
msgstr ""

#: e-conf.c:4490 e-conf.c:4616
msgid "Modifier"
msgstr ""

#: e-conf.c:4491 e-conf.c:4599
msgid "Key"
msgstr ""

#: e-conf.c:4492 e-conf.c:4764
msgid "Action to perform"
msgstr ""

#: e-conf.c:4493
msgid "Optional parameters"
msgstr ""

#: e-conf.c:4499
msgid "List of keyboard shortcuts"
msgstr ""

#: e-conf.c:4511
msgid "New"
msgstr ""

#. -----------------------------------------------------
#: e-conf.c:4581
msgid "Edit current selected keyboard shortcut"
msgstr ""

#: e-conf.c:4609
msgid "Change..."
msgstr ""

#: e-conf.c:4623
msgid "NONE"
msgstr ""

#: e-conf.c:4646
msgid "CTRL & ALT"
msgstr ""

#: e-conf.c:4652
msgid "CTRL & SHIFT"
msgstr ""

#: e-conf.c:4658
msgid "ALT & SHIFT"
msgstr ""

#: e-conf.c:4664
msgid "CTRL & ALT & SHIFT"
msgstr ""

#: e-conf.c:4694
msgid "WIN & SHIFT"
msgstr ""

#: e-conf.c:4700
msgid "WIN & CTRL"
msgstr ""

#: e-conf.c:4706
msgid "WIN & ALT"
msgstr ""

#: e-conf.c:4712
msgid "MOD4 & SHIFT"
msgstr ""

#: e-conf.c:4718
msgid "MOD4 & CTRL"
msgstr ""

#: e-conf.c:4724
msgid "MOD4 & CTRL & SHIFT"
msgstr ""

#: e-conf.c:4730
msgid "MOD5 & SHIFT"
msgstr ""

#: e-conf.c:4736
msgid "MOD5 & CTRL"
msgstr ""

#: e-conf.c:4789
msgid "Options for Action:"
msgstr ""

#: e-conf.c:4866
msgid "Basic Options"
msgstr ""

#: e-conf.c:4873
#, fuzzy
msgid "Desktops"
msgstr "S�parer les bureaux"

#: e-conf.c:4880
msgid "Behavior"
msgstr ""

#: e-conf.c:4887
msgid "Audio"
msgstr ""

#: e-conf.c:4894
msgid "Special FX"
msgstr ""

#: e-conf.c:4901
#, fuzzy
msgid "Backgrounds"
msgstr "Pas de fond d'�cran"

#: e-conf.c:4944
msgid "Themes"
msgstr ""

#: e-conf.c:4951
#, fuzzy
msgid "Shortcuts"
msgstr "Clic Clavier"

#: e-conf.c:5039
msgid "Enlightenment Configuration Editor"
msgstr ""

#: e-conf.c:5056
msgid "Quit"
msgstr ""

#: e-conf.c:5059
msgid ""
"You are not running Enlightenment\n"
"\n"
"This window manager has to be running in order\n"
"to configure it.\n"
"\n"
msgstr ""

#: e-conf.c:5087
msgid "Scanning Enlightenment Configuration..."
msgstr ""

#: e-conf_filesel.c:62
msgid "Select a file to save"
msgstr "S�lection du fichier � sauvegarder"

#: e-conf_filesel.c:75 e-conf_filesel.c:127 e-conf_filesel.c:558
msgid "Select a file to load"
msgstr "S�lection du fichier � charger"

#: e-conf_filesel.c:102
msgid "Set Printout settings"
msgstr ""

#: e-conf_filesel.c:585
msgid "Saved Image Settings"
msgstr ""

#: e-conf_filesel.c:592
msgid "Print Command:"
msgstr "Commande d'Impression:"

#: e-conf_filesel.c:605
msgid "Print"
msgstr "Imprimer"

#: e-conf_filesel.c:623
msgid "Image Format:"
msgstr "Format d'image:"

#: e-conf_filesel.c:694
msgid "Quality Settings"
msgstr "Param�tres de qualit�"

#: e-conf_filesel.c:706
msgid "Page Settings"
msgstr "Param�tres de la pagination"

#: e-conf_filesel.c:779
msgid "Color"
msgstr "Couleur"

#: e-conf_filesel.c:792 e-conf_filesel.c:793
msgid "Scaling Factor:"
msgstr "Facteur d'agrandissement:"

#: e-conf_filesel.c:822
msgid "Select a file"
msgstr "S�lectionnez un fichier"

#: e-conf_filesel.c:833
msgid "Load all files in directory"
msgstr "Charger tous les fichiers du r�pertoire"

#: e-conf_filesel.c:847
msgid "Use Previews"
msgstr "Utiliser les pr�visualisations"

#: e-conf_filesel.c:857
msgid "Image Information"
msgstr "Informations sur l'image"

#~ msgid "Edit gradient colours..."
#~ msgstr "Editer les couleurs de gradients..."

#~ msgid "Use tooltips & timeout (sec)"
#~ msgstr "Utilisation des bulles d'aide et d�lai d'activation (sec)"

#~ msgid "capplet-command to be run."
#~ msgstr "capplet-command � lancer."

#~ msgid "Initialize session settings"
#~ msgstr "Initialiser les param�tres de session"

#~ msgid "IOR"
#~ msgstr "IOR"

#~ msgid "IOR of the control-center"
#~ msgstr "IOR du control-center"

#~ msgid "XID"
#~ msgstr "XID"

#~ msgid "CAPID"
#~ msgstr "CAPID"

#~ msgid "Multi-capplet id."
#~ msgstr "id. Multi-capplet"

#~ msgid "ID"
#~ msgstr "ID"

#~ msgid "id of the capplet -- assigned by the control-center"
#~ msgstr "id de la capplet -- assign�e par le control-center"

#~ msgid "Help"
#~ msgstr "Aide"

#~ msgid "Desktop Properties manager."
#~ msgstr "Gestionnaires des Propri�t�s du Bureau."

#~ msgid "Desktop Manager"
#~ msgstr "Gestionnaire du bureau"

#~ msgid "Remove"
#~ msgstr "Enlever"

#~ msgid "Help browser (new window)"
#~ msgstr "Help browser (nouvelle fen�tre)"

#~ msgid "Help browser"
#~ msgstr "Navigateur d'Aide Gnome"

#~ msgid "Netscape (new window)"
#~ msgstr "Netscape (nouvelle fen�tre)"

#~ msgid "Netscape"
#~ msgstr "Netscape"

#~ msgid "handler:"
#~ msgstr "gestionnaire:"

#~ msgid "Protocol"
#~ msgstr "Protocole"

#~ msgid "MDI notebook tab position"
#~ msgstr "Position des onglets des carnets MDI"

#~ msgid "Default MDI mode"
#~ msgstr "Mode MDI par d�faut"

#~ msgid "Menu items have icons"
#~ msgstr "Entr�es des menus avec icones"

#~ msgid "Dialog buttons have icons"
#~ msgstr "Boutons de dialogue avec icones"

#~ msgid "Statusbar is interactive when possible"
#~ msgstr "Barre d'�tat interactive si possible"

#~ msgid "Toolbars have text labels"
#~ msgstr "Barres d'outils avec des labels textuels"

#~ msgid "Toolbars have line separators"
#~ msgstr "Barres d'outils avec des lignes de s�paration"

#~ msgid "Toolbar buttons have relieved border"
#~ msgstr "Bords avec relief pour les boutons des barres d'outils"

#~ msgid "Toolbars have relieved border"
#~ msgstr "Bords avec relief pour les barres d'outils"

#~ msgid "Menubars have relieved border"
#~ msgstr "Bords avec relief pour les barres de menu"

#~ msgid "Can detach and move menubars"
#~ msgstr "Barres de menu d�tachables et mobiles"

#~ msgid "Can detach and move toolbars"
#~ msgstr "Barres d'outils d�tachables et mobiles"

#~ msgid "Place dialogs over application window when possible"
#~ msgstr "Placer les dialogues au-dessus de l'application si possible"

#~ msgid "Use statusbar instead of dialog when possible"
#~ msgstr "Utiliser la barre d'�tat � la place des dialogues si possible"

#~ msgid "Dialog hints"
#~ msgstr "Dialogues"

#~ msgid "Dialog position"
#~ msgstr "Position du dialogue"

#~ msgid "Dialog buttons"
#~ msgstr "Boutons de dialogue"

#~ msgid "Top"
#~ msgstr "Haut"

#~ msgid "Right"
#~ msgstr "Droite"

#~ msgid "Left"
#~ msgstr "Gauche"

#~ msgid "Modal"
#~ msgstr "Modal"

#~ msgid "Notebook"
#~ msgstr "Carnet"

#~ msgid "Dialogs are treated specially by window manager"
#~ msgstr ""
#~ "Les dialogues sont trait�s de mani�re sp�ciale par le gestionnaire de "
#~ "fen�tres"

#~ msgid "Dialogs are like other windows"
#~ msgstr "Les dialogues sont comme les autres fen�tres"

#~ msgid "Let window manager decide"
#~ msgstr "Laisser le gestionnaire de fen�tres d�cider"

#~ msgid "Right-justify buttons"
#~ msgstr "Justification � droite des boutons"

#~ msgid "Left-justify buttons"
#~ msgstr "Justification � gauche des boutons"

#~ msgid "Put buttons on edges"
#~ msgstr "Mettre les boutons sur les bords"

#~ msgid "Spread buttons out"
#~ msgstr "Espacer les boutons"

#~ msgid "Default Gtk setting [FIXME - Describe this better]"
#~ msgstr "Configuration de Gtk par d�faut"

#~ msgid "Theme Information"
#~ msgstr "Informations sur le th�me"

#~ msgid ""
#~ "Install new\n"
#~ "theme..."
#~ msgstr ""
#~ "Installer le\n"
#~ "nouveau th�me..."

#~ msgid ""
#~ "Auto\n"
#~ "Preview"
#~ msgstr ""
#~ "Auto\n"
#~ "Pr�visualisation"

#~ msgid "Available Themes"
#~ msgstr "Th�mes Disponibles"

#~ msgid "Sound Events"
#~ msgstr "Ev�nements Sonores"

#~ msgid "Select sound file"
#~ msgstr "S�lection du fichier son"

#~ msgid "Play"
#~ msgstr "Jouer"

#~ msgid "General"
#~ msgstr "G�n�ral"

#~ msgid "Sounds for events"
#~ msgstr "Effets sonores"

#~ msgid "GNOME sound support"
#~ msgstr "support pour le son GNOME"

#~ msgid "File to Play"
#~ msgstr "Fichier � jouer"

#~ msgid "Event"
#~ msgstr "Ev�nement"

#~ msgid "Category"
#~ msgstr "Cat�gorie"

#~ msgid "Screen Saver Demo"
#~ msgstr "D�mo de l'�conomiseur d'�cran"

#~ msgid " minutes after screen saver has started."
#~ msgstr " minutes apr�s le d�marrage de l'�conomiseur d'�cran."

#~ msgid "Shutdown monitor "
#~ msgstr "Moniteur d'extinction "

#~ msgid " Normal"
#~ msgstr " Normale"

#~ msgid "Low "
#~ msgstr "Basse "

#~ msgid "Priority:"
#~ msgstr "Priorit�:"

#~ msgid " Minutes."
#~ msgstr " Minutes."

#~ msgid "Start After "
#~ msgstr "D�marrer apr�s "

#~ msgid "Screen Saver Settings"
#~ msgstr "Param�tres de l'�conomiseur d'�cran"

#~ msgid ""
#~ "Pressing this button will popup a dialogbox that will help you setup the "
#~ "current screensaver."
#~ msgstr ""
#~ "Ce bouton ouvre une fen�tre de dialogue qui vous aidera a configurer "
#~ "l'�conomiseur d'�cran."

#~ msgid "Screen Saver"
#~ msgstr "Economiseur d'�cran"

#~ msgid "Settings"
#~ msgstr "Param�tres"

#~ msgid "Author: UNKNOWN"
#~ msgstr "Auteur: INCONNU"

#~ msgid "Author:"
#~ msgstr "Auteur:"

#~ msgid "About:"
#~ msgstr "A Propos:"

#~ msgid "Preview"
#~ msgstr "Pr�visualisation"

#~ msgid " Settings"
#~ msgstr " Param�tres"

#~ msgid "Settings..."
#~ msgstr "Param�tres..."

#~ msgid "Use power management."
#~ msgstr "Utiliser la gestion de l'�nergie."

#~ msgid "Require Password"
#~ msgstr "N�cessite un mot de passe"

#~ msgid "Small"
#~ msgstr "Faible"

#~ msgid "Large"
#~ msgstr "Importante"

#~ msgid "Threshold"
#~ msgstr "R�activit�"

#~ msgid "Slow"
#~ msgstr "Lent"

#~ msgid "Fast"
#~ msgstr "Rapide"

#~ msgid "Acceleration"
#~ msgstr "Acc�l�ration"

#~ msgid "Test settings"
#~ msgstr "Test du param�trage"

#~ msgid "Click volume"
#~ msgstr "Volume des clicks"

#~ msgid "Click on keypress"
#~ msgstr "Frappe sonore"

#~ msgid "Repeat Delay"
#~ msgstr "D�lai avant r�p�tition"

#~ msgid "Repeat rate"
#~ msgstr "Vitesse de r�p�tition"

#~ msgid "Enable auto-repeat"
#~ msgstr "Activer l'Auto-r�p�tition"

#~ msgid "Auto-repeat"
#~ msgstr "Auto-r�p�tition"

#~ msgid "Duration (ms)"
#~ msgstr "Dur�e (ms)"

#~ msgid "Volume"
#~ msgstr "Volume"

#~ msgid "Keyboard Bell"
#~ msgstr "Clic Clavier"

#~ msgid "Display wallpaper: tiled, centered, scaled or ratio"
#~ msgstr "Affichage du papier peint: mosaique, centr�, redimentionn� ou ratio"

#~ msgid "Use a gradient fill for the background"
#~ msgstr "Utiliser un d�grad� de couleur pour le fond d'�cran"

#~ msgid "Use a solid fill for the background"
#~ msgstr "Utiliser une couleur uniforme pour le fond d'�cran"

#~ msgid "Gradient orientation: vertical or horizontal"
#~ msgstr "Orientation du gradient: vertical ou horizontal"

#~ msgid "ORIENT"
#~ msgstr "ORIENT"

#~ msgid "Specifies end background color for gradient"
#~ msgstr "Sp�ficier la couleur de fin de gradient pour la couleur de fond"

#~ msgid "Specifies the background color"
#~ msgstr "Sp�cifier une couleur de fond d'�cran"

#~ msgid "COLOR"
#~ msgstr "COULEUR"

#~ msgid "Sets the wallpaper to the value specified"
#~ msgstr "Utiliser la couleur sp�cifi�e pour le fond d'�cran"

#~ msgid "IMAGE"
#~ msgstr "IMAGE"

#~ msgid "Set parameters from saved state and exit"
#~ msgstr "Restaurer les param�tres sauvegarder et quitter"

#~ msgid "Scaled (keep aspect)"
#~ msgstr "Redimentionn�e (en conservant l'aspect)"

#~ msgid "none"
#~ msgstr "aucun"

#~ msgid " Browse... "
#~ msgstr " Parcourir... "

#~ msgid "Wallpaper"
#~ msgstr "Papier peint"

#~ msgid "Wallpaper Selection"
#~ msgstr "S�lection du papier peint"

#~ msgid "Horizontal"
#~ msgstr "Horizontal"

#~ msgid "Vertical"
#~ msgstr "Vertical"

#~ msgid "Flat"
#~ msgstr "Uniforme"

#, fuzzy
#~ msgid "Mouse Cursors"
#~ msgstr "Boutons de la souris"

#, fuzzy
#~ msgid "Colours"
#~ msgstr "Couleur"

#, fuzzy
#~ msgid "Virtual Desktop"
#~ msgstr "S�parer les bureaux"

#~ msgid "Revert"
#~ msgstr "Enlever"

#~ msgid "Try"
#~ msgstr "Essayer"
