/*****************             Electirc Eyes           ***********************/
/* This software is Copyright (C) 1998 but The Rasterman (Carsten Haitzler). */
/* This software falls under the GNU Public License. Please read the COPYING */
/* file for more information                                                 */
/*****************************************************************************/

#ifndef __EE_FILESEL_H__
#define __EE_FILESEL_H__

#include "config.h"
#include <gtk/gtk.h>
#include <gdk_imlib.h>

#ifdef __cplusplus
extern              "C"
{
#endif                          /* __cplusplus */
GtkWidget          *ee_filesel_new(void);
void                ee_filesel_show_savedialog(GtkWidget *widget);
void                ee_filesel_hide_savedialog(GtkWidget *widget);
void                ee_filesel_set_printout(GtkWidget *widget);
void                ee_filesel_unset_printout(GtkWidget *widget);
gchar *             ee_filesel_get_filename(GtkWidget *widget);
void                ee_filesel_set_filename(GtkWidget *widget, gchar *file);
void                ee_filesel_set_save_image(GtkWidget *widget, GdkImlibImage *im);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif
